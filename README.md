Profiling urine microRNA in LAMA2-CMD

These scripts/analyses pertain UPPMAX project up_072, performed as part of my
PhD studies.
The goal of the project was to profile muscle miRNAs in a mouse model (dy3K/dy3K) of laminin alpha 2 chain-deficient muscular dystrophy (LAMA2-CMD). To do so we had
skeletal muscle homogenate samples sequenced for small RNAs on the Ion Torrent
Proton platform.
The raw data was received as BAM files mapped to mm10; FASTQ files were generated
from these using BEDtools (bam2fq.sh).
FASTQ were mapped to the mature miRNA reference from miRBase using bowtie
(bowtie_align.sh), sorted (sam_sort.sh) and read counts generated using HTSeq
(htseq.sh). Differential expression was done in R with DESeq2.
Tarbase was used for microRNA target prediction and target genes were further
analysed for enrichment. microRNA target enrichment is currently not very
informative as target genes are computationally predicted, yielding hundreds of
targets for many miRNAs. To circumvent this to some degree I've used Tarbase, a
curated database of experimentally validated miRNA targets. Tarbase also has tissue
information, allowing us to limit the analyses to a tissue of interest, if wanted.
Therefore, I've filtered Tarbase to keep only the mouse skeletal muscle subset.
Data processing and analyses were performed on the Aurora HPC cluster of LUNARC.
