var networks = {"up_mirna.graphml": {
  "format_version" : "1.0",
  "generated_by" : "cytoscape-3.6.0",
  "target_cytoscapejs_version" : "~2.1",
  "data" : {
    "shared_name" : "up_mirna.graphml",
    "name" : "up_mirna.graphml",
    "SUID" : 52,
    "__Annotations" : [ "" ],
    "selected" : true
  },
  "elements" : {
    "nodes" : [ {
      "data" : {
        "id" : "201",
        "shared_name" : "Nav1",
        "name" : "Nav1",
        "degree_layout" : 3,
        "SUID" : 201,
        "selected" : false
      },
      "position" : {
        "x" : -194.20626105408815,
        "y" : 190.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "200",
        "shared_name" : "E2f6",
        "name" : "E2f6",
        "degree_layout" : 3,
        "SUID" : 200,
        "selected" : false
      },
      "position" : {
        "x" : -158.20626105408815,
        "y" : 570.4148665694329
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "199",
        "shared_name" : "Letmd1",
        "name" : "Letmd1",
        "degree_layout" : 3,
        "SUID" : 199,
        "selected" : false
      },
      "position" : {
        "x" : 476.79373894591185,
        "y" : 390.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "198",
        "shared_name" : "Rgs2",
        "name" : "Rgs2",
        "degree_layout" : 1,
        "SUID" : 198,
        "selected" : false
      },
      "position" : {
        "x" : -833.244041106839,
        "y" : 601.9148665694329
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "197",
        "shared_name" : "Fzd3",
        "name" : "Fzd3",
        "degree_layout" : 1,
        "SUID" : 197,
        "selected" : false
      },
      "position" : {
        "x" : -887.9751510804635,
        "y" : 507.11780334047535
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "196",
        "shared_name" : "Rhoc",
        "name" : "Rhoc",
        "degree_layout" : 1,
        "SUID" : 196,
        "selected" : false
      },
      "position" : {
        "x" : -557.9373710277127,
        "y" : -1057.3821966595247
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "195",
        "shared_name" : "Camsap2",
        "name" : "Camsap2",
        "degree_layout" : 6,
        "SUID" : 195,
        "selected" : false
      },
      "position" : {
        "x" : -40.206261054088145,
        "y" : 35.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "194",
        "shared_name" : "Zfp451",
        "name" : "Zfp451",
        "degree_layout" : 5,
        "SUID" : 194,
        "selected" : false
      },
      "position" : {
        "x" : -176.20626105408815,
        "y" : 111.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "193",
        "shared_name" : "Pabpn1",
        "name" : "Pabpn1",
        "degree_layout" : 2,
        "SUID" : 193,
        "selected" : false
      },
      "position" : {
        "x" : 23.793738945911855,
        "y" : 628.4148665694329
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "192",
        "shared_name" : "Nras",
        "name" : "Nras",
        "degree_layout" : 2,
        "SUID" : 192,
        "selected" : false
      },
      "position" : {
        "x" : -228.41581765251726,
        "y" : 259.8042883930475
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "191",
        "shared_name" : "Ccdc141",
        "name" : "Ccdc141",
        "degree_layout" : 2,
        "SUID" : 191,
        "selected" : false
      },
      "position" : {
        "x" : 941.2937389459119,
        "y" : -1383.5851334305673
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "190",
        "shared_name" : "S100pbp",
        "name" : "S100pbp",
        "degree_layout" : 4,
        "SUID" : 190,
        "selected" : false
      },
      "position" : {
        "x" : -61.206261054088145,
        "y" : 520.4148665694327
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "189",
        "shared_name" : "Uggt1",
        "name" : "Uggt1",
        "degree_layout" : 1,
        "SUID" : 189,
        "selected" : false
      },
      "position" : {
        "x" : 822.5666720438182,
        "y" : 154.20012305012688
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "188",
        "shared_name" : "Hfe2",
        "name" : "Hfe2",
        "degree_layout" : 1,
        "SUID" : 188,
        "selected" : false
      },
      "position" : {
        "x" : -997.4373710277125,
        "y" : 507.11780334047535
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "187",
        "shared_name" : "Arf6",
        "name" : "Arf6",
        "degree_layout" : 2,
        "SUID" : 187,
        "selected" : false
      },
      "position" : {
        "x" : 243.79373894591185,
        "y" : 620.4148665694329
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "186",
        "shared_name" : "Ppig",
        "name" : "Ppig",
        "degree_layout" : 1,
        "SUID" : 186,
        "selected" : false
      },
      "position" : {
        "x" : -653.9373710277125,
        "y" : 1175.1178033404753
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "185",
        "shared_name" : "Ptprf",
        "name" : "Ptprf",
        "degree_layout" : 2,
        "SUID" : 185,
        "selected" : false
      },
      "position" : {
        "x" : 315.79373894591185,
        "y" : -362.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "184",
        "shared_name" : "Ywhab",
        "name" : "Ywhab",
        "degree_layout" : 2,
        "SUID" : 184,
        "selected" : false
      },
      "position" : {
        "x" : 383.79373894591185,
        "y" : 761.4148665694329
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "183",
        "shared_name" : "Rgmb",
        "name" : "Rgmb",
        "degree_layout" : 1,
        "SUID" : 183,
        "selected" : false
      },
      "position" : {
        "x" : -612.6684810013372,
        "y" : -962.5851334305671
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "182",
        "shared_name" : "Ncl",
        "name" : "Ncl",
        "degree_layout" : 5,
        "SUID" : 182,
        "selected" : false
      },
      "position" : {
        "x" : -94.20626105408815,
        "y" : 162.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "181",
        "shared_name" : "Ptgr2",
        "name" : "Ptgr2",
        "degree_layout" : 1,
        "SUID" : 181,
        "selected" : false
      },
      "position" : {
        "x" : -557.9373710277126,
        "y" : -867.7880702016098
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "180",
        "shared_name" : "Piezo1",
        "name" : "Piezo1",
        "degree_layout" : 1,
        "SUID" : 180,
        "selected" : false
      },
      "position" : {
        "x" : -1052.168481001337,
        "y" : 601.9148665694329
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "179",
        "shared_name" : "Cd59a",
        "name" : "Cd59a",
        "degree_layout" : 2,
        "SUID" : 179,
        "selected" : false
      },
      "position" : {
        "x" : 1398.7469714856777,
        "y" : -365.1731078245921
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "178",
        "shared_name" : "Dusp14",
        "name" : "Dusp14",
        "degree_layout" : 1,
        "SUID" : 178,
        "selected" : false
      },
      "position" : {
        "x" : -448.47515108046366,
        "y" : -867.7880702016098
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "177",
        "shared_name" : "Fam102b",
        "name" : "Fam102b",
        "degree_layout" : 3,
        "SUID" : 177,
        "selected" : false
      },
      "position" : {
        "x" : -384.20626105408815,
        "y" : 382.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "176",
        "shared_name" : "Sc5d",
        "name" : "Sc5d",
        "degree_layout" : 1,
        "SUID" : 176,
        "selected" : false
      },
      "position" : {
        "x" : -503.20626105408826,
        "y" : -743.6606935360692
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "175",
        "shared_name" : "Itpripl2",
        "name" : "Itpripl2",
        "degree_layout" : 4,
        "SUID" : 175,
        "selected" : false
      },
      "position" : {
        "x" : -29.206261054088145,
        "y" : -0.585133430567339
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "174",
        "shared_name" : "Arl4c",
        "name" : "Arl4c",
        "degree_layout" : 3,
        "SUID" : 174,
        "selected" : false
      },
      "position" : {
        "x" : 324.79373894591185,
        "y" : -121.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "173",
        "shared_name" : "BC027231",
        "name" : "BC027231",
        "degree_layout" : 2,
        "SUID" : 173,
        "selected" : false
      },
      "position" : {
        "x" : -668.2062610540881,
        "y" : 235.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "172",
        "shared_name" : "Lrig3",
        "name" : "Lrig3",
        "degree_layout" : 2,
        "SUID" : 172,
        "selected" : false
      },
      "position" : {
        "x" : 1031.3347649518762,
        "y" : -1321.3389188342305
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "171",
        "shared_name" : "Ccar2",
        "name" : "Ccar2",
        "degree_layout" : 1,
        "SUID" : 171,
        "selected" : false
      },
      "position" : {
        "x" : -997.4373710277125,
        "y" : 696.71192979839
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "170",
        "shared_name" : "Hsd17b11",
        "name" : "Hsd17b11",
        "degree_layout" : 2,
        "SUID" : 170,
        "selected" : false
      },
      "position" : {
        "x" : 1251.7631574403351,
        "y" : -419.76091318794283
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "169",
        "shared_name" : "Ncam1",
        "name" : "Ncam1",
        "degree_layout" : 1,
        "SUID" : 169,
        "selected" : false
      },
      "position" : {
        "x" : -887.9751510804635,
        "y" : 696.71192979839
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "168",
        "shared_name" : "AI464131",
        "name" : "AI464131",
        "degree_layout" : 1,
        "SUID" : 168,
        "selected" : false
      },
      "position" : {
        "x" : -708.6684810013373,
        "y" : 1269.914866569433
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "167",
        "shared_name" : "Ankrd10",
        "name" : "Ankrd10",
        "degree_layout" : 1,
        "SUID" : 167,
        "selected" : false
      },
      "position" : {
        "x" : -942.7062610540881,
        "y" : 820.8393064639308
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "166",
        "shared_name" : "Nr4a1",
        "name" : "Nr4a1",
        "degree_layout" : 1,
        "SUID" : 166,
        "selected" : false
      },
      "position" : {
        "x" : -653.9373710277125,
        "y" : 1364.71192979839
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "165",
        "shared_name" : "Slc25a11",
        "name" : "Slc25a11",
        "degree_layout" : 1,
        "SUID" : 165,
        "selected" : false
      },
      "position" : {
        "x" : -544.4751510804637,
        "y" : 1364.71192979839
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "164",
        "shared_name" : "Wfdc1",
        "name" : "Wfdc1",
        "degree_layout" : 1,
        "SUID" : 164,
        "selected" : false
      },
      "position" : {
        "x" : 920.3631182257475,
        "y" : 107.10383675933326
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "163",
        "shared_name" : "Ank1",
        "name" : "Ank1",
        "degree_layout" : 1,
        "SUID" : 163,
        "selected" : false
      },
      "position" : {
        "x" : 316.39040390634864,
        "y" : 108.91486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "162",
        "shared_name" : "Sfrp1",
        "name" : "Sfrp1",
        "degree_layout" : 1,
        "SUID" : 162,
        "selected" : false
      },
      "position" : {
        "x" : 988.0403588327597,
        "y" : 22.239261275684385
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "161",
        "shared_name" : "Deptor",
        "name" : "Deptor",
        "degree_layout" : 1,
        "SUID" : 161,
        "selected" : false
      },
      "position" : {
        "x" : -380.2818211595901,
        "y" : 1269.914866569433
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "160",
        "shared_name" : "Atp6ap2",
        "name" : "Atp6ap2",
        "degree_layout" : 2,
        "SUID" : 160,
        "selected" : false
      },
      "position" : {
        "x" : 1251.0624725802675,
        "y" : -312.51041829243036
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "159",
        "shared_name" : "Camk2g",
        "name" : "Camk2g",
        "degree_layout" : 4,
        "SUID" : 159,
        "selected" : false
      },
      "position" : {
        "x" : -110.20626105408815,
        "y" : 93.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "158",
        "shared_name" : "Slc25a25",
        "name" : "Slc25a25",
        "degree_layout" : 1,
        "SUID" : 158,
        "selected" : false
      },
      "position" : {
        "x" : 1012.194086355749,
        "y" : -83.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "157",
        "shared_name" : "Arhgap28",
        "name" : "Arhgap28",
        "degree_layout" : 2,
        "SUID" : 157,
        "selected" : false
      },
      "position" : {
        "x" : 1003.5399535422491,
        "y" : -1473.6261594365315
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "156",
        "shared_name" : "Tmod1",
        "name" : "Tmod1",
        "degree_layout" : 1,
        "SUID" : 156,
        "selected" : false
      },
      "position" : {
        "x" : -393.7440411068392,
        "y" : -772.9910069726525
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "155",
        "shared_name" : "Slc16a2",
        "name" : "Slc16a2",
        "degree_layout" : 1,
        "SUID" : 155,
        "selected" : false
      },
      "position" : {
        "x" : 988.0403588327597,
        "y" : -189.4095281368186
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "154",
        "shared_name" : "Rlim",
        "name" : "Rlim",
        "degree_layout" : 3,
        "SUID" : 154,
        "selected" : false
      },
      "position" : {
        "x" : -266.20626105408815,
        "y" : 81.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "153",
        "shared_name" : "Tirap",
        "name" : "Tirap",
        "degree_layout" : 4,
        "SUID" : 153,
        "selected" : false
      },
      "position" : {
        "x" : 57.793738945911855,
        "y" : 385.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "152",
        "shared_name" : "Rab43",
        "name" : "Rab43",
        "degree_layout" : 1,
        "SUID" : 152,
        "selected" : false
      },
      "position" : {
        "x" : 920.3631182257475,
        "y" : -274.2741036204677
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "151",
        "shared_name" : "Gpd2",
        "name" : "Gpd2",
        "degree_layout" : 2,
        "SUID" : 151,
        "selected" : false
      },
      "position" : {
        "x" : 851.252712939948,
        "y" : -1445.8313480269042
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "150",
        "shared_name" : "Vegfa",
        "name" : "Vegfa",
        "degree_layout" : 1,
        "SUID" : 150,
        "selected" : false
      },
      "position" : {
        "x" : -489.74404110683906,
        "y" : 1080.3207401115178
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "149",
        "shared_name" : "Tshz3",
        "name" : "Tshz3",
        "degree_layout" : 1,
        "SUID" : 149,
        "selected" : false
      },
      "position" : {
        "x" : -313.6121345961735,
        "y" : -853.1229134833181
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "148",
        "shared_name" : "Lrrfip1",
        "name" : "Lrrfip1",
        "degree_layout" : 4,
        "SUID" : 148,
        "selected" : false
      },
      "position" : {
        "x" : -16.206261054088145,
        "y" : 244.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "147",
        "shared_name" : "Fem1a",
        "name" : "Fem1a",
        "degree_layout" : 2,
        "SUID" : 147,
        "selected" : false
      },
      "position" : {
        "x" : 1319.4650060884203,
        "y" : -229.90138110961675
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "146",
        "shared_name" : "Slco3a1",
        "name" : "Slco3a1",
        "degree_layout" : 1,
        "SUID" : 146,
        "selected" : false
      },
      "position" : {
        "x" : -833.244041106839,
        "y" : 791.5089930273475
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "145",
        "shared_name" : "Clip4",
        "name" : "Clip4",
        "degree_layout" : 1,
        "SUID" : 145,
        "selected" : false
      },
      "position" : {
        "x" : -284.2818211595902,
        "y" : -962.5851334305673
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "144",
        "shared_name" : "Ppm1l",
        "name" : "Ppm1l",
        "degree_layout" : 1,
        "SUID" : 144,
        "selected" : false
      },
      "position" : {
        "x" : 822.5666720438187,
        "y" : -321.3703899112613
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "143",
        "shared_name" : "4833439L19Rik",
        "name" : "4833439L19Rik",
        "degree_layout" : 1,
        "SUID" : 143,
        "selected" : false
      },
      "position" : {
        "x" : -313.6121345961735,
        "y" : -1072.0473533778163
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "142",
        "shared_name" : "mmu-miR-133a-3p",
        "name" : "mmu-miR-133a-3p",
        "degree_layout" : 33,
        "SUID" : 142,
        "selected" : false
      },
      "position" : {
        "x" : 394.79373894591185,
        "y" : 318.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "141",
        "shared_name" : "Sbno2",
        "name" : "Sbno2",
        "degree_layout" : 1,
        "SUID" : 141,
        "selected" : false
      },
      "position" : {
        "x" : -753.1121345961733,
        "y" : 711.3770865166816
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "140",
        "shared_name" : "Ppfibp1",
        "name" : "Ppfibp1",
        "degree_layout" : 2,
        "SUID" : 140,
        "selected" : false
      },
      "position" : {
        "x" : 1424.9644523268473,
        "y" : -210.58748823452197
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "139",
        "shared_name" : "Srpr",
        "name" : "Srpr",
        "degree_layout" : 1,
        "SUID" : 139,
        "selected" : false
      },
      "position" : {
        "x" : -708.6684810013373,
        "y" : 1080.3207401115178
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "138",
        "shared_name" : "Myo5a",
        "name" : "Myo5a",
        "degree_layout" : 5,
        "SUID" : 138,
        "selected" : false
      },
      "position" : {
        "x" : -118.20626105408815,
        "y" : 202.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "137",
        "shared_name" : "Ptbp3",
        "name" : "Ptbp3",
        "degree_layout" : 4,
        "SUID" : 137,
        "selected" : false
      },
      "position" : {
        "x" : 296.79373894591185,
        "y" : 415.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "136",
        "shared_name" : "Myo9b",
        "name" : "Myo9b",
        "degree_layout" : 4,
        "SUID" : 136,
        "selected" : false
      },
      "position" : {
        "x" : 193.79373894591185,
        "y" : -104.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "135",
        "shared_name" : "Lpin2",
        "name" : "Lpin2",
        "degree_layout" : 1,
        "SUID" : 135,
        "selected" : false
      },
      "position" : {
        "x" : -723.78182115959,
        "y" : 601.9148665694329
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "134",
        "shared_name" : "B3galnt2",
        "name" : "B3galnt2",
        "degree_layout" : 1,
        "SUID" : 134,
        "selected" : false
      },
      "position" : {
        "x" : 714.0208058480055,
        "y" : -321.3703899112613
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "133",
        "shared_name" : "Slc25a12",
        "name" : "Slc25a12",
        "degree_layout" : 1,
        "SUID" : 133,
        "selected" : false
      },
      "position" : {
        "x" : 616.2243596660762,
        "y" : -274.27410362046794
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "132",
        "shared_name" : "Vamp1",
        "name" : "Vamp1",
        "degree_layout" : 2,
        "SUID" : 132,
        "selected" : false
      },
      "position" : {
        "x" : -187.70626105408815,
        "y" : 361.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "131",
        "shared_name" : "Golm1",
        "name" : "Golm1",
        "degree_layout" : 1,
        "SUID" : 131,
        "selected" : false
      },
      "position" : {
        "x" : -393.74404110683906,
        "y" : -1152.179259888482
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "130",
        "shared_name" : "Zfp697",
        "name" : "Zfp697",
        "degree_layout" : 2,
        "SUID" : 130,
        "selected" : false
      },
      "position" : {
        "x" : 1518.1964479048477,
        "y" : -263.6059247934129
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "129",
        "shared_name" : "Tns3",
        "name" : "Tns3",
        "degree_layout" : 1,
        "SUID" : 129,
        "selected" : false
      },
      "position" : {
        "x" : 548.547119059064,
        "y" : -189.40952813681906
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "128",
        "shared_name" : "Smad3",
        "name" : "Smad3",
        "degree_layout" : 1,
        "SUID" : 128,
        "selected" : false
      },
      "position" : {
        "x" : -753.1121345961733,
        "y" : 492.45264662218347
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "127",
        "shared_name" : "Ahnak",
        "name" : "Ahnak",
        "degree_layout" : 3,
        "SUID" : 127,
        "selected" : false
      },
      "position" : {
        "x" : 262.79373894591185,
        "y" : -132.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "126",
        "shared_name" : "Got1",
        "name" : "Got1",
        "degree_layout" : 2,
        "SUID" : 126,
        "selected" : false
      },
      "position" : {
        "x" : 1555.5367059332618,
        "y" : -364.14877508609584
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "125",
        "shared_name" : "Erbb3",
        "name" : "Erbb3",
        "degree_layout" : 1,
        "SUID" : 125,
        "selected" : false
      },
      "position" : {
        "x" : -833.244041106839,
        "y" : 412.3207401115178
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "124",
        "shared_name" : "Eid1",
        "name" : "Eid1",
        "degree_layout" : 1,
        "SUID" : 124,
        "selected" : false
      },
      "position" : {
        "x" : 524.3933915360747,
        "y" : -83.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "123",
        "shared_name" : "Tnc",
        "name" : "Tnc",
        "degree_layout" : 2,
        "SUID" : 123,
        "selected" : false
      },
      "position" : {
        "x" : 246.79373894591185,
        "y" : -234.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "122",
        "shared_name" : "Dnaja2",
        "name" : "Dnaja2",
        "degree_layout" : 1,
        "SUID" : 122,
        "selected" : false
      },
      "position" : {
        "x" : -503.20626105408803,
        "y" : -1181.5095733250653
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "121",
        "shared_name" : "Trp53bp1",
        "name" : "Trp53bp1",
        "degree_layout" : 1,
        "SUID" : 121,
        "selected" : false
      },
      "position" : {
        "x" : -818.1307009485861,
        "y" : 1269.914866569433
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "120",
        "shared_name" : "Ric8",
        "name" : "Ric8",
        "degree_layout" : 1,
        "SUID" : 120,
        "selected" : false
      },
      "position" : {
        "x" : -708.6684810013369,
        "y" : 1459.5089930273475
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "119",
        "shared_name" : "Matr3",
        "name" : "Matr3",
        "degree_layout" : 1,
        "SUID" : 119,
        "selected" : false
      },
      "position" : {
        "x" : 548.547119059064,
        "y" : 22.239261275684385
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "118",
        "shared_name" : "Tmem86a",
        "name" : "Tmem86a",
        "degree_layout" : 4,
        "SUID" : 118,
        "selected" : false
      },
      "position" : {
        "x" : 100.79373894591185,
        "y" : 482.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "117",
        "shared_name" : "Vim",
        "name" : "Vim",
        "degree_layout" : 1,
        "SUID" : 117,
        "selected" : false
      },
      "position" : {
        "x" : -942.7062610540879,
        "y" : 382.9904266749345
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "116",
        "shared_name" : "mmu-miR-133b-3p",
        "name" : "mmu-miR-133b-3p",
        "degree_layout" : 34,
        "SUID" : 116,
        "selected" : false
      },
      "position" : {
        "x" : 256.79373894591185,
        "y" : -68.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "115",
        "shared_name" : "Tmem127",
        "name" : "Tmem127",
        "degree_layout" : 5,
        "SUID" : 115,
        "selected" : false
      },
      "position" : {
        "x" : -39.206261054088145,
        "y" : 101.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "114",
        "shared_name" : "Scrn3",
        "name" : "Scrn3",
        "degree_layout" : 1,
        "SUID" : 114,
        "selected" : false
      },
      "position" : {
        "x" : -612.668481001337,
        "y" : -1152.1792598884822
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "113",
        "shared_name" : "Gpc6",
        "name" : "Gpc6",
        "degree_layout" : 1,
        "SUID" : 113,
        "selected" : false
      },
      "position" : {
        "x" : 616.2243596660762,
        "y" : 107.10383675933326
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "112",
        "shared_name" : "Cdc37l1",
        "name" : "Cdc37l1",
        "degree_layout" : 2,
        "SUID" : 112,
        "selected" : false
      },
      "position" : {
        "x" : -944.2062610540881,
        "y" : 80.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "111",
        "shared_name" : "Mtus1",
        "name" : "Mtus1",
        "degree_layout" : 3,
        "SUID" : 111,
        "selected" : false
      },
      "position" : {
        "x" : 158.79373894591185,
        "y" : 372.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "110",
        "shared_name" : "mmu-miR-154-5p",
        "name" : "mmu-miR-154-5p",
        "degree_layout" : 18,
        "SUID" : 110,
        "selected" : false
      },
      "position" : {
        "x" : 206.92818395909967,
        "y" : 108.91486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "109",
        "shared_name" : "Myo10",
        "name" : "Myo10",
        "degree_layout" : 1,
        "SUID" : 109,
        "selected" : false
      },
      "position" : {
        "x" : -1052.1684810013369,
        "y" : 412.3207401115178
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "108",
        "shared_name" : "Spop",
        "name" : "Spop",
        "degree_layout" : 2,
        "SUID" : 108,
        "selected" : false
      },
      "position" : {
        "x" : -1168.2062610540881,
        "y" : -18.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "107",
        "shared_name" : "Atp2b1",
        "name" : "Atp2b1",
        "degree_layout" : 1,
        "SUID" : 107,
        "selected" : false
      },
      "position" : {
        "x" : -489.74404110683906,
        "y" : 1459.5089930273475
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "106",
        "shared_name" : "Cd99l2",
        "name" : "Cd99l2",
        "degree_layout" : 1,
        "SUID" : 106,
        "selected" : false
      },
      "position" : {
        "x" : -692.8003875120028,
        "y" : -1072.0473533778165
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "105",
        "shared_name" : "Fam13b",
        "name" : "Fam13b",
        "degree_layout" : 1,
        "SUID" : 105,
        "selected" : false
      },
      "position" : {
        "x" : -1132.3003875120025,
        "y" : 492.45264662218324
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "104",
        "shared_name" : "Arfip2",
        "name" : "Arfip2",
        "degree_layout" : 1,
        "SUID" : 104,
        "selected" : false
      },
      "position" : {
        "x" : -1161.630700948586,
        "y" : 601.9148665694324
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "103",
        "shared_name" : "mmu-miR-1a-3p",
        "name" : "mmu-miR-1a-3p",
        "degree_layout" : 47,
        "SUID" : 103,
        "selected" : false
      },
      "position" : {
        "x" : 768.2937389459119,
        "y" : -83.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "102",
        "shared_name" : "Homer1",
        "name" : "Homer1",
        "degree_layout" : 3,
        "SUID" : 102,
        "selected" : false
      },
      "position" : {
        "x" : 219.79373894591185,
        "y" : 384.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "101",
        "shared_name" : "Pkd1",
        "name" : "Pkd1",
        "degree_layout" : 4,
        "SUID" : 101,
        "selected" : false
      },
      "position" : {
        "x" : 11.793738945911855,
        "y" : 158.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "100",
        "shared_name" : "Arid5a",
        "name" : "Arid5a",
        "degree_layout" : 3,
        "SUID" : 100,
        "selected" : false
      },
      "position" : {
        "x" : 573.7937389459119,
        "y" : 309.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "99",
        "shared_name" : "Fat1",
        "name" : "Fat1",
        "degree_layout" : 5,
        "SUID" : 99,
        "selected" : false
      },
      "position" : {
        "x" : 164.79373894591185,
        "y" : 286.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "98",
        "shared_name" : "Htt",
        "name" : "Htt",
        "degree_layout" : 2,
        "SUID" : 98,
        "selected" : false
      },
      "position" : {
        "x" : -146.99670445565926,
        "y" : 463.0254447458176
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "97",
        "shared_name" : "Rtn4",
        "name" : "Rtn4",
        "degree_layout" : 1,
        "SUID" : 97,
        "selected" : false
      },
      "position" : {
        "x" : 823.0248489195362,
        "y" : -178.38219665952465
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "96",
        "shared_name" : "Atl2",
        "name" : "Atl2",
        "degree_layout" : 2,
        "SUID" : 96,
        "selected" : false
      },
      "position" : {
        "x" : 879.047524349575,
        "y" : -1293.5441074246035
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "95",
        "shared_name" : "C2cd2",
        "name" : "C2cd2",
        "degree_layout" : 1,
        "SUID" : 95,
        "selected" : false
      },
      "position" : {
        "x" : -722.1307009485861,
        "y" : -962.5851334305676
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "94",
        "shared_name" : "Tgfbr1",
        "name" : "Tgfbr1",
        "degree_layout" : 2,
        "SUID" : 94,
        "selected" : false
      },
      "position" : {
        "x" : -1362.2062610540881,
        "y" : 996.4148665694329
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "93",
        "shared_name" : "Hnrnpu",
        "name" : "Hnrnpu",
        "degree_layout" : 3,
        "SUID" : 93,
        "selected" : false
      },
      "position" : {
        "x" : 469.79373894591185,
        "y" : -476.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "92",
        "shared_name" : "Ggnbp1",
        "name" : "Ggnbp1",
        "degree_layout" : 1,
        "SUID" : 92,
        "selected" : false
      },
      "position" : {
        "x" : -692.8003875120028,
        "y" : -853.1229134833184
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "91",
        "shared_name" : "Ndufa8",
        "name" : "Ndufa8",
        "degree_layout" : 1,
        "SUID" : 91,
        "selected" : false
      },
      "position" : {
        "x" : 713.5626289722875,
        "y" : -178.38219665952465
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "90",
        "shared_name" : "Cyfip1",
        "name" : "Cyfip1",
        "degree_layout" : 1,
        "SUID" : 90,
        "selected" : false
      },
      "position" : {
        "x" : -1132.3003875120028,
        "y" : 711.3770865166816
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "89",
        "shared_name" : "Srr",
        "name" : "Srr",
        "degree_layout" : 1,
        "SUID" : 89,
        "selected" : false
      },
      "position" : {
        "x" : 658.8315189986631,
        "y" : -83.58513343056711
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "88",
        "shared_name" : "Aldh5a1",
        "name" : "Aldh5a1",
        "degree_layout" : 1,
        "SUID" : 88,
        "selected" : false
      },
      "position" : {
        "x" : 713.5626289722875,
        "y" : 11.211929798389974
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "87",
        "shared_name" : "mmu-miR-499-5p",
        "name" : "mmu-miR-499-5p",
        "degree_layout" : 32,
        "SUID" : 87,
        "selected" : false
      },
      "position" : {
        "x" : -599.2062610540883,
        "y" : 1269.914866569433
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "86",
        "shared_name" : "Fam46a",
        "name" : "Fam46a",
        "degree_layout" : 2,
        "SUID" : 86,
        "selected" : false
      },
      "position" : {
        "x" : 1519.5133046898516,
        "y" : -465.1709220513119
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "85",
        "shared_name" : "Reep1",
        "name" : "Reep1",
        "degree_layout" : 1,
        "SUID" : 85,
        "selected" : false
      },
      "position" : {
        "x" : -489.74404110683906,
        "y" : 1269.914866569433
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "84",
        "shared_name" : "Nono",
        "name" : "Nono",
        "degree_layout" : 1,
        "SUID" : 84,
        "selected" : false
      },
      "position" : {
        "x" : 152.19707398547507,
        "y" : 14.117803340475348
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "83",
        "shared_name" : "Clcn3",
        "name" : "Clcn3",
        "degree_layout" : 4,
        "SUID" : 83,
        "selected" : false
      },
      "position" : {
        "x" : 99.79373894591185,
        "y" : 306.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "82",
        "shared_name" : "Arpp21",
        "name" : "Arpp21",
        "degree_layout" : 1,
        "SUID" : 82,
        "selected" : false
      },
      "position" : {
        "x" : -544.4751510804637,
        "y" : 1175.1178033404753
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "81",
        "shared_name" : "Maf",
        "name" : "Maf",
        "degree_layout" : 3,
        "SUID" : 81,
        "selected" : false
      },
      "position" : {
        "x" : -359.20626105408815,
        "y" : 257.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "80",
        "shared_name" : "2610008E11Rik",
        "name" : "2610008E11Rik",
        "degree_layout" : 3,
        "SUID" : 80,
        "selected" : false
      },
      "position" : {
        "x" : -269.20626105408815,
        "y" : 164.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "79",
        "shared_name" : "Foxp1",
        "name" : "Foxp1",
        "degree_layout" : 4,
        "SUID" : 79,
        "selected" : false
      },
      "position" : {
        "x" : 76.79373894591185,
        "y" : 433.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "78",
        "shared_name" : "Prkab2",
        "name" : "Prkab2",
        "degree_layout" : 1,
        "SUID" : 78,
        "selected" : false
      },
      "position" : {
        "x" : -612.6684810013372,
        "y" : -772.9910069726527
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "77",
        "shared_name" : "mmu-miR-20a-5p",
        "name" : "mmu-miR-20a-5p",
        "degree_layout" : 47,
        "SUID" : 77,
        "selected" : false
      },
      "position" : {
        "x" : -503.20626105408815,
        "y" : -962.5851334305673
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "76",
        "shared_name" : "Apbb2",
        "name" : "Apbb2",
        "degree_layout" : 3,
        "SUID" : 76,
        "selected" : false
      },
      "position" : {
        "x" : -404.20626105408815,
        "y" : 316.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "75",
        "shared_name" : "Ccng1",
        "name" : "Ccng1",
        "degree_layout" : 1,
        "SUID" : 75,
        "selected" : false
      },
      "position" : {
        "x" : -393.74404110683906,
        "y" : -962.5851334305673
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "74",
        "shared_name" : "Zdhhc18",
        "name" : "Zdhhc18",
        "degree_layout" : 1,
        "SUID" : 74,
        "selected" : false
      },
      "position" : {
        "x" : 823.0248489195362,
        "y" : 11.211929798389974
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "73",
        "shared_name" : "Ar",
        "name" : "Ar",
        "degree_layout" : 1,
        "SUID" : 73,
        "selected" : false
      },
      "position" : {
        "x" : 152.19707398547507,
        "y" : 203.71192979838997
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "72",
        "shared_name" : "Fnip2",
        "name" : "Fnip2",
        "degree_layout" : 3,
        "SUID" : 72,
        "selected" : false
      },
      "position" : {
        "x" : 209.79373894591185,
        "y" : 471.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "71",
        "shared_name" : "Sar1b",
        "name" : "Sar1b",
        "degree_layout" : 1,
        "SUID" : 71,
        "selected" : false
      },
      "position" : {
        "x" : -448.47515108046366,
        "y" : -1057.3821966595247
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "70",
        "shared_name" : "Tcirg1",
        "name" : "Tcirg1",
        "degree_layout" : 3,
        "SUID" : 70,
        "selected" : false
      },
      "position" : {
        "x" : 44.793738945911855,
        "y" : 228.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "69",
        "shared_name" : "Cacnb1",
        "name" : "Cacnb1",
        "degree_layout" : 3,
        "SUID" : 69,
        "selected" : false
      },
      "position" : {
        "x" : 158.79373894591185,
        "y" : 408.41486656943266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "68",
        "shared_name" : "Csnk1g3",
        "name" : "Csnk1g3",
        "degree_layout" : 2,
        "SUID" : 68,
        "selected" : false
      },
      "position" : {
        "x" : 1426.981993971918,
        "y" : -519.4029803879334
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "67",
        "shared_name" : "Cdca4",
        "name" : "Cdca4",
        "degree_layout" : 4,
        "SUID" : 67,
        "selected" : false
      },
      "position" : {
        "x" : 110.79373894591185,
        "y" : -105.58513343056734
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "66",
        "shared_name" : "Chst11",
        "name" : "Chst11",
        "degree_layout" : 1,
        "SUID" : 66,
        "selected" : false
      },
      "position" : {
        "x" : 714.0208058480055,
        "y" : 154.20012305012665
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "65",
        "shared_name" : "Adsl",
        "name" : "Adsl",
        "degree_layout" : 1,
        "SUID" : 65,
        "selected" : false
      },
      "position" : {
        "x" : -1052.168481001337,
        "y" : 791.5089930273471
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "64",
        "shared_name" : "Dlat",
        "name" : "Dlat",
        "degree_layout" : 2,
        "SUID" : 64,
        "selected" : false
      },
      "position" : {
        "x" : 1321.239202435352,
        "y" : -501.4691672780639
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "63",
        "shared_name" : "mmu-miR-22-3p",
        "name" : "mmu-miR-22-3p",
        "degree_layout" : 44,
        "SUID" : 63,
        "selected" : false
      },
      "position" : {
        "x" : -942.7062610540881,
        "y" : 601.9148665694329
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "62",
        "shared_name" : "Fam20a",
        "name" : "Fam20a",
        "degree_layout" : 1,
        "SUID" : 62,
        "selected" : false
      },
      "position" : {
        "x" : 877.7559588931606,
        "y" : -83.58513343056734
      },
      "selected" : false
    } ],
    "edges" : [ {
      "data" : {
        "id" : "456",
        "source" : "201",
        "target" : "63",
        "shared_name" : "Nav1 (-) mmu-miR-22-3p",
        "name" : "Nav1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 456,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "455",
        "source" : "201",
        "target" : "103",
        "shared_name" : "Nav1 (-) mmu-miR-1a-3p",
        "name" : "Nav1 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 455,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "454",
        "source" : "201",
        "target" : "77",
        "shared_name" : "Nav1 (-) mmu-miR-20a-5p",
        "name" : "Nav1 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 454,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "453",
        "source" : "200",
        "target" : "63",
        "shared_name" : "E2f6 (-) mmu-miR-22-3p",
        "name" : "E2f6 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 453,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "452",
        "source" : "200",
        "target" : "87",
        "shared_name" : "E2f6 (-) mmu-miR-499-5p",
        "name" : "E2f6 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 452,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "451",
        "source" : "200",
        "target" : "110",
        "shared_name" : "E2f6 (-) mmu-miR-154-5p",
        "name" : "E2f6 (-) mmu-miR-154-5p",
        "interaction" : "-",
        "SUID" : 451,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "450",
        "source" : "199",
        "target" : "142",
        "shared_name" : "Letmd1 (-) mmu-miR-133a-3p",
        "name" : "Letmd1 (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 450,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "449",
        "source" : "199",
        "target" : "116",
        "shared_name" : "Letmd1 (-) mmu-miR-133b-3p",
        "name" : "Letmd1 (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 449,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "448",
        "source" : "199",
        "target" : "103",
        "shared_name" : "Letmd1 (-) mmu-miR-1a-3p",
        "name" : "Letmd1 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 448,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "447",
        "source" : "198",
        "target" : "63",
        "shared_name" : "Rgs2 (-) mmu-miR-22-3p",
        "name" : "Rgs2 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 447,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "446",
        "source" : "197",
        "target" : "63",
        "shared_name" : "Fzd3 (-) mmu-miR-22-3p",
        "name" : "Fzd3 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 446,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "445",
        "source" : "196",
        "target" : "77",
        "shared_name" : "Rhoc (-) mmu-miR-20a-5p",
        "name" : "Rhoc (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 445,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "444",
        "source" : "195",
        "target" : "116",
        "shared_name" : "Camsap2 (-) mmu-miR-133b-3p",
        "name" : "Camsap2 (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 444,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "443",
        "source" : "195",
        "target" : "103",
        "shared_name" : "Camsap2 (-) mmu-miR-1a-3p",
        "name" : "Camsap2 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 443,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "442",
        "source" : "195",
        "target" : "142",
        "shared_name" : "Camsap2 (-) mmu-miR-133a-3p",
        "name" : "Camsap2 (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 442,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "441",
        "source" : "195",
        "target" : "63",
        "shared_name" : "Camsap2 (-) mmu-miR-22-3p",
        "name" : "Camsap2 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 441,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "440",
        "source" : "195",
        "target" : "77",
        "shared_name" : "Camsap2 (-) mmu-miR-20a-5p",
        "name" : "Camsap2 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 440,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "439",
        "source" : "195",
        "target" : "110",
        "shared_name" : "Camsap2 (-) mmu-miR-154-5p",
        "name" : "Camsap2 (-) mmu-miR-154-5p",
        "interaction" : "-",
        "SUID" : 439,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "438",
        "source" : "194",
        "target" : "77",
        "shared_name" : "Zfp451 (-) mmu-miR-20a-5p",
        "name" : "Zfp451 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 438,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "437",
        "source" : "194",
        "target" : "110",
        "shared_name" : "Zfp451 (-) mmu-miR-154-5p",
        "name" : "Zfp451 (-) mmu-miR-154-5p",
        "interaction" : "-",
        "SUID" : 437,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "436",
        "source" : "194",
        "target" : "63",
        "shared_name" : "Zfp451 (-) mmu-miR-22-3p",
        "name" : "Zfp451 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 436,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "435",
        "source" : "194",
        "target" : "87",
        "shared_name" : "Zfp451 (-) mmu-miR-499-5p",
        "name" : "Zfp451 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 435,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "434",
        "source" : "194",
        "target" : "103",
        "shared_name" : "Zfp451 (-) mmu-miR-1a-3p",
        "name" : "Zfp451 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 434,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "433",
        "source" : "193",
        "target" : "87",
        "shared_name" : "Pabpn1 (-) mmu-miR-499-5p",
        "name" : "Pabpn1 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 433,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "432",
        "source" : "193",
        "target" : "110",
        "shared_name" : "Pabpn1 (-) mmu-miR-154-5p",
        "name" : "Pabpn1 (-) mmu-miR-154-5p",
        "interaction" : "-",
        "SUID" : 432,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "431",
        "source" : "192",
        "target" : "63",
        "shared_name" : "Nras (-) mmu-miR-22-3p",
        "name" : "Nras (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 431,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "430",
        "source" : "192",
        "target" : "103",
        "shared_name" : "Nras (-) mmu-miR-1a-3p",
        "name" : "Nras (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 430,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "429",
        "source" : "191",
        "target" : "103",
        "shared_name" : "Ccdc141 (-) mmu-miR-1a-3p",
        "name" : "Ccdc141 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 429,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "428",
        "source" : "191",
        "target" : "77",
        "shared_name" : "Ccdc141 (-) mmu-miR-20a-5p",
        "name" : "Ccdc141 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 428,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "427",
        "source" : "190",
        "target" : "142",
        "shared_name" : "S100pbp (-) mmu-miR-133a-3p",
        "name" : "S100pbp (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 427,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "426",
        "source" : "190",
        "target" : "116",
        "shared_name" : "S100pbp (-) mmu-miR-133b-3p",
        "name" : "S100pbp (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 426,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "425",
        "source" : "190",
        "target" : "63",
        "shared_name" : "S100pbp (-) mmu-miR-22-3p",
        "name" : "S100pbp (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 425,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "424",
        "source" : "190",
        "target" : "87",
        "shared_name" : "S100pbp (-) mmu-miR-499-5p",
        "name" : "S100pbp (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 424,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "423",
        "source" : "189",
        "target" : "103",
        "shared_name" : "Uggt1 (-) mmu-miR-1a-3p",
        "name" : "Uggt1 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 423,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "422",
        "source" : "188",
        "target" : "63",
        "shared_name" : "Hfe2 (-) mmu-miR-22-3p",
        "name" : "Hfe2 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 422,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "421",
        "source" : "187",
        "target" : "87",
        "shared_name" : "Arf6 (-) mmu-miR-499-5p",
        "name" : "Arf6 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 421,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "420",
        "source" : "187",
        "target" : "103",
        "shared_name" : "Arf6 (-) mmu-miR-1a-3p",
        "name" : "Arf6 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 420,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "419",
        "source" : "186",
        "target" : "87",
        "shared_name" : "Ppig (-) mmu-miR-499-5p",
        "name" : "Ppig (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 419,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "418",
        "source" : "185",
        "target" : "77",
        "shared_name" : "Ptprf (-) mmu-miR-20a-5p",
        "name" : "Ptprf (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 418,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "417",
        "source" : "185",
        "target" : "110",
        "shared_name" : "Ptprf (-) mmu-miR-154-5p",
        "name" : "Ptprf (-) mmu-miR-154-5p",
        "interaction" : "-",
        "SUID" : 417,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "416",
        "source" : "184",
        "target" : "87",
        "shared_name" : "Ywhab (-) mmu-miR-499-5p",
        "name" : "Ywhab (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 416,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "415",
        "source" : "184",
        "target" : "103",
        "shared_name" : "Ywhab (-) mmu-miR-1a-3p",
        "name" : "Ywhab (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 415,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "414",
        "source" : "183",
        "target" : "77",
        "shared_name" : "Rgmb (-) mmu-miR-20a-5p",
        "name" : "Rgmb (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 414,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "413",
        "source" : "182",
        "target" : "142",
        "shared_name" : "Ncl (-) mmu-miR-133a-3p",
        "name" : "Ncl (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 413,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "412",
        "source" : "182",
        "target" : "110",
        "shared_name" : "Ncl (-) mmu-miR-154-5p",
        "name" : "Ncl (-) mmu-miR-154-5p",
        "interaction" : "-",
        "SUID" : 412,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "411",
        "source" : "182",
        "target" : "116",
        "shared_name" : "Ncl (-) mmu-miR-133b-3p",
        "name" : "Ncl (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 411,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "410",
        "source" : "182",
        "target" : "87",
        "shared_name" : "Ncl (-) mmu-miR-499-5p",
        "name" : "Ncl (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 410,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "409",
        "source" : "182",
        "target" : "77",
        "shared_name" : "Ncl (-) mmu-miR-20a-5p",
        "name" : "Ncl (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 409,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "408",
        "source" : "181",
        "target" : "77",
        "shared_name" : "Ptgr2 (-) mmu-miR-20a-5p",
        "name" : "Ptgr2 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 408,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "407",
        "source" : "180",
        "target" : "63",
        "shared_name" : "Piezo1 (-) mmu-miR-22-3p",
        "name" : "Piezo1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 407,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "406",
        "source" : "179",
        "target" : "142",
        "shared_name" : "Cd59a (-) mmu-miR-133a-3p",
        "name" : "Cd59a (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 406,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "405",
        "source" : "179",
        "target" : "116",
        "shared_name" : "Cd59a (-) mmu-miR-133b-3p",
        "name" : "Cd59a (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 405,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "404",
        "source" : "178",
        "target" : "77",
        "shared_name" : "Dusp14 (-) mmu-miR-20a-5p",
        "name" : "Dusp14 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 404,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "403",
        "source" : "177",
        "target" : "63",
        "shared_name" : "Fam102b (-) mmu-miR-22-3p",
        "name" : "Fam102b (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 403,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "402",
        "source" : "177",
        "target" : "87",
        "shared_name" : "Fam102b (-) mmu-miR-499-5p",
        "name" : "Fam102b (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 402,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "401",
        "source" : "177",
        "target" : "103",
        "shared_name" : "Fam102b (-) mmu-miR-1a-3p",
        "name" : "Fam102b (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 401,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "400",
        "source" : "176",
        "target" : "77",
        "shared_name" : "Sc5d (-) mmu-miR-20a-5p",
        "name" : "Sc5d (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 400,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "399",
        "source" : "175",
        "target" : "142",
        "shared_name" : "Itpripl2 (-) mmu-miR-133a-3p",
        "name" : "Itpripl2 (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 399,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "398",
        "source" : "175",
        "target" : "116",
        "shared_name" : "Itpripl2 (-) mmu-miR-133b-3p",
        "name" : "Itpripl2 (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 398,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "397",
        "source" : "175",
        "target" : "63",
        "shared_name" : "Itpripl2 (-) mmu-miR-22-3p",
        "name" : "Itpripl2 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 397,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "396",
        "source" : "175",
        "target" : "77",
        "shared_name" : "Itpripl2 (-) mmu-miR-20a-5p",
        "name" : "Itpripl2 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 396,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "395",
        "source" : "174",
        "target" : "142",
        "shared_name" : "Arl4c (-) mmu-miR-133a-3p",
        "name" : "Arl4c (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 395,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "394",
        "source" : "174",
        "target" : "116",
        "shared_name" : "Arl4c (-) mmu-miR-133b-3p",
        "name" : "Arl4c (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 394,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "393",
        "source" : "174",
        "target" : "77",
        "shared_name" : "Arl4c (-) mmu-miR-20a-5p",
        "name" : "Arl4c (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 393,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "392",
        "source" : "173",
        "target" : "87",
        "shared_name" : "BC027231 (-) mmu-miR-499-5p",
        "name" : "BC027231 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 392,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "391",
        "source" : "173",
        "target" : "77",
        "shared_name" : "BC027231 (-) mmu-miR-20a-5p",
        "name" : "BC027231 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 391,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "390",
        "source" : "172",
        "target" : "103",
        "shared_name" : "Lrig3 (-) mmu-miR-1a-3p",
        "name" : "Lrig3 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 390,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "389",
        "source" : "172",
        "target" : "77",
        "shared_name" : "Lrig3 (-) mmu-miR-20a-5p",
        "name" : "Lrig3 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 389,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "388",
        "source" : "171",
        "target" : "63",
        "shared_name" : "Ccar2 (-) mmu-miR-22-3p",
        "name" : "Ccar2 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 388,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "387",
        "source" : "170",
        "target" : "142",
        "shared_name" : "Hsd17b11 (-) mmu-miR-133a-3p",
        "name" : "Hsd17b11 (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 387,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "386",
        "source" : "170",
        "target" : "116",
        "shared_name" : "Hsd17b11 (-) mmu-miR-133b-3p",
        "name" : "Hsd17b11 (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 386,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "385",
        "source" : "169",
        "target" : "63",
        "shared_name" : "Ncam1 (-) mmu-miR-22-3p",
        "name" : "Ncam1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 385,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "384",
        "source" : "168",
        "target" : "87",
        "shared_name" : "AI464131 (-) mmu-miR-499-5p",
        "name" : "AI464131 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 384,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "383",
        "source" : "167",
        "target" : "63",
        "shared_name" : "Ankrd10 (-) mmu-miR-22-3p",
        "name" : "Ankrd10 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 383,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "382",
        "source" : "166",
        "target" : "87",
        "shared_name" : "Nr4a1 (-) mmu-miR-499-5p",
        "name" : "Nr4a1 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 382,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "381",
        "source" : "165",
        "target" : "87",
        "shared_name" : "Slc25a11 (-) mmu-miR-499-5p",
        "name" : "Slc25a11 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 381,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "380",
        "source" : "164",
        "target" : "103",
        "shared_name" : "Wfdc1 (-) mmu-miR-1a-3p",
        "name" : "Wfdc1 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 380,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "379",
        "source" : "163",
        "target" : "110",
        "shared_name" : "Ank1 (-) mmu-miR-154-5p",
        "name" : "Ank1 (-) mmu-miR-154-5p",
        "interaction" : "-",
        "SUID" : 379,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "378",
        "source" : "162",
        "target" : "103",
        "shared_name" : "Sfrp1 (-) mmu-miR-1a-3p",
        "name" : "Sfrp1 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 378,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "377",
        "source" : "161",
        "target" : "87",
        "shared_name" : "Deptor (-) mmu-miR-499-5p",
        "name" : "Deptor (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 377,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "376",
        "source" : "160",
        "target" : "142",
        "shared_name" : "Atp6ap2 (-) mmu-miR-133a-3p",
        "name" : "Atp6ap2 (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 376,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "375",
        "source" : "160",
        "target" : "116",
        "shared_name" : "Atp6ap2 (-) mmu-miR-133b-3p",
        "name" : "Atp6ap2 (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 375,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "374",
        "source" : "159",
        "target" : "142",
        "shared_name" : "Camk2g (-) mmu-miR-133a-3p",
        "name" : "Camk2g (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 374,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "373",
        "source" : "159",
        "target" : "116",
        "shared_name" : "Camk2g (-) mmu-miR-133b-3p",
        "name" : "Camk2g (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 373,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "372",
        "source" : "159",
        "target" : "63",
        "shared_name" : "Camk2g (-) mmu-miR-22-3p",
        "name" : "Camk2g (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 372,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "371",
        "source" : "159",
        "target" : "110",
        "shared_name" : "Camk2g (-) mmu-miR-154-5p",
        "name" : "Camk2g (-) mmu-miR-154-5p",
        "interaction" : "-",
        "SUID" : 371,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "370",
        "source" : "158",
        "target" : "103",
        "shared_name" : "Slc25a25 (-) mmu-miR-1a-3p",
        "name" : "Slc25a25 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 370,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "369",
        "source" : "157",
        "target" : "103",
        "shared_name" : "Arhgap28 (-) mmu-miR-1a-3p",
        "name" : "Arhgap28 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 369,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "368",
        "source" : "157",
        "target" : "77",
        "shared_name" : "Arhgap28 (-) mmu-miR-20a-5p",
        "name" : "Arhgap28 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 368,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "367",
        "source" : "156",
        "target" : "77",
        "shared_name" : "Tmod1 (-) mmu-miR-20a-5p",
        "name" : "Tmod1 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 367,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "366",
        "source" : "155",
        "target" : "103",
        "shared_name" : "Slc16a2 (-) mmu-miR-1a-3p",
        "name" : "Slc16a2 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 366,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "365",
        "source" : "154",
        "target" : "63",
        "shared_name" : "Rlim (-) mmu-miR-22-3p",
        "name" : "Rlim (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 365,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "364",
        "source" : "154",
        "target" : "77",
        "shared_name" : "Rlim (-) mmu-miR-20a-5p",
        "name" : "Rlim (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 364,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "363",
        "source" : "154",
        "target" : "110",
        "shared_name" : "Rlim (-) mmu-miR-154-5p",
        "name" : "Rlim (-) mmu-miR-154-5p",
        "interaction" : "-",
        "SUID" : 363,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "362",
        "source" : "153",
        "target" : "142",
        "shared_name" : "Tirap (-) mmu-miR-133a-3p",
        "name" : "Tirap (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 362,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "361",
        "source" : "153",
        "target" : "116",
        "shared_name" : "Tirap (-) mmu-miR-133b-3p",
        "name" : "Tirap (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 361,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "360",
        "source" : "153",
        "target" : "63",
        "shared_name" : "Tirap (-) mmu-miR-22-3p",
        "name" : "Tirap (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 360,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "359",
        "source" : "153",
        "target" : "87",
        "shared_name" : "Tirap (-) mmu-miR-499-5p",
        "name" : "Tirap (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 359,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "358",
        "source" : "152",
        "target" : "103",
        "shared_name" : "Rab43 (-) mmu-miR-1a-3p",
        "name" : "Rab43 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 358,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "357",
        "source" : "151",
        "target" : "103",
        "shared_name" : "Gpd2 (-) mmu-miR-1a-3p",
        "name" : "Gpd2 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 357,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "356",
        "source" : "151",
        "target" : "77",
        "shared_name" : "Gpd2 (-) mmu-miR-20a-5p",
        "name" : "Gpd2 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 356,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "355",
        "source" : "150",
        "target" : "87",
        "shared_name" : "Vegfa (-) mmu-miR-499-5p",
        "name" : "Vegfa (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 355,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "354",
        "source" : "149",
        "target" : "77",
        "shared_name" : "Tshz3 (-) mmu-miR-20a-5p",
        "name" : "Tshz3 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 354,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "353",
        "source" : "148",
        "target" : "142",
        "shared_name" : "Lrrfip1 (-) mmu-miR-133a-3p",
        "name" : "Lrrfip1 (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 353,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "352",
        "source" : "148",
        "target" : "116",
        "shared_name" : "Lrrfip1 (-) mmu-miR-133b-3p",
        "name" : "Lrrfip1 (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 352,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "351",
        "source" : "148",
        "target" : "87",
        "shared_name" : "Lrrfip1 (-) mmu-miR-499-5p",
        "name" : "Lrrfip1 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 351,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "350",
        "source" : "148",
        "target" : "77",
        "shared_name" : "Lrrfip1 (-) mmu-miR-20a-5p",
        "name" : "Lrrfip1 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 350,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "349",
        "source" : "147",
        "target" : "142",
        "shared_name" : "Fem1a (-) mmu-miR-133a-3p",
        "name" : "Fem1a (-) mmu-miR-133a-3p",
        "interaction" : "-",
        "SUID" : 349,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "348",
        "source" : "147",
        "target" : "116",
        "shared_name" : "Fem1a (-) mmu-miR-133b-3p",
        "name" : "Fem1a (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 348,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "347",
        "source" : "146",
        "target" : "63",
        "shared_name" : "Slco3a1 (-) mmu-miR-22-3p",
        "name" : "Slco3a1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 347,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "346",
        "source" : "145",
        "target" : "77",
        "shared_name" : "Clip4 (-) mmu-miR-20a-5p",
        "name" : "Clip4 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 346,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "345",
        "source" : "144",
        "target" : "103",
        "shared_name" : "Ppm1l (-) mmu-miR-1a-3p",
        "name" : "Ppm1l (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 345,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "344",
        "source" : "143",
        "target" : "77",
        "shared_name" : "4833439L19Rik (-) mmu-miR-20a-5p",
        "name" : "4833439L19Rik (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 344,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "343",
        "source" : "142",
        "target" : "86",
        "shared_name" : "mmu-miR-133a-3p (-) Fam46a",
        "name" : "mmu-miR-133a-3p (-) Fam46a",
        "interaction" : "-",
        "SUID" : 343,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "342",
        "source" : "142",
        "target" : "130",
        "shared_name" : "mmu-miR-133a-3p (-) Zfp697",
        "name" : "mmu-miR-133a-3p (-) Zfp697",
        "interaction" : "-",
        "SUID" : 342,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "341",
        "source" : "142",
        "target" : "68",
        "shared_name" : "mmu-miR-133a-3p (-) Csnk1g3",
        "name" : "mmu-miR-133a-3p (-) Csnk1g3",
        "interaction" : "-",
        "SUID" : 341,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "340",
        "source" : "142",
        "target" : "127",
        "shared_name" : "mmu-miR-133a-3p (-) Ahnak",
        "name" : "mmu-miR-133a-3p (-) Ahnak",
        "interaction" : "-",
        "SUID" : 340,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "339",
        "source" : "142",
        "target" : "83",
        "shared_name" : "mmu-miR-133a-3p (-) Clcn3",
        "name" : "mmu-miR-133a-3p (-) Clcn3",
        "interaction" : "-",
        "SUID" : 339,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "338",
        "source" : "142",
        "target" : "99",
        "shared_name" : "mmu-miR-133a-3p (-) Fat1",
        "name" : "mmu-miR-133a-3p (-) Fat1",
        "interaction" : "-",
        "SUID" : 338,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "337",
        "source" : "142",
        "target" : "79",
        "shared_name" : "mmu-miR-133a-3p (-) Foxp1",
        "name" : "mmu-miR-133a-3p (-) Foxp1",
        "interaction" : "-",
        "SUID" : 337,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "336",
        "source" : "142",
        "target" : "118",
        "shared_name" : "mmu-miR-133a-3p (-) Tmem86a",
        "name" : "mmu-miR-133a-3p (-) Tmem86a",
        "interaction" : "-",
        "SUID" : 336,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "335",
        "source" : "142",
        "target" : "115",
        "shared_name" : "mmu-miR-133a-3p (-) Tmem127",
        "name" : "mmu-miR-133a-3p (-) Tmem127",
        "interaction" : "-",
        "SUID" : 335,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "334",
        "source" : "142",
        "target" : "69",
        "shared_name" : "mmu-miR-133a-3p (-) Cacnb1",
        "name" : "mmu-miR-133a-3p (-) Cacnb1",
        "interaction" : "-",
        "SUID" : 334,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "333",
        "source" : "142",
        "target" : "72",
        "shared_name" : "mmu-miR-133a-3p (-) Fnip2",
        "name" : "mmu-miR-133a-3p (-) Fnip2",
        "interaction" : "-",
        "SUID" : 333,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "332",
        "source" : "142",
        "target" : "70",
        "shared_name" : "mmu-miR-133a-3p (-) Tcirg1",
        "name" : "mmu-miR-133a-3p (-) Tcirg1",
        "interaction" : "-",
        "SUID" : 332,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "331",
        "source" : "142",
        "target" : "140",
        "shared_name" : "mmu-miR-133a-3p (-) Ppfibp1",
        "name" : "mmu-miR-133a-3p (-) Ppfibp1",
        "interaction" : "-",
        "SUID" : 331,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "330",
        "source" : "142",
        "target" : "67",
        "shared_name" : "mmu-miR-133a-3p (-) Cdca4",
        "name" : "mmu-miR-133a-3p (-) Cdca4",
        "interaction" : "-",
        "SUID" : 330,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "329",
        "source" : "142",
        "target" : "111",
        "shared_name" : "mmu-miR-133a-3p (-) Mtus1",
        "name" : "mmu-miR-133a-3p (-) Mtus1",
        "interaction" : "-",
        "SUID" : 329,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "328",
        "source" : "142",
        "target" : "64",
        "shared_name" : "mmu-miR-133a-3p (-) Dlat",
        "name" : "mmu-miR-133a-3p (-) Dlat",
        "interaction" : "-",
        "SUID" : 328,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "327",
        "source" : "142",
        "target" : "137",
        "shared_name" : "mmu-miR-133a-3p (-) Ptbp3",
        "name" : "mmu-miR-133a-3p (-) Ptbp3",
        "interaction" : "-",
        "SUID" : 327,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "326",
        "source" : "142",
        "target" : "100",
        "shared_name" : "mmu-miR-133a-3p (-) Arid5a",
        "name" : "mmu-miR-133a-3p (-) Arid5a",
        "interaction" : "-",
        "SUID" : 326,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "325",
        "source" : "142",
        "target" : "126",
        "shared_name" : "mmu-miR-133a-3p (-) Got1",
        "name" : "mmu-miR-133a-3p (-) Got1",
        "interaction" : "-",
        "SUID" : 325,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "324",
        "source" : "142",
        "target" : "136",
        "shared_name" : "mmu-miR-133a-3p (-) Myo9b",
        "name" : "mmu-miR-133a-3p (-) Myo9b",
        "interaction" : "-",
        "SUID" : 324,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "323",
        "source" : "141",
        "target" : "63",
        "shared_name" : "Sbno2 (-) mmu-miR-22-3p",
        "name" : "Sbno2 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 323,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "322",
        "source" : "140",
        "target" : "116",
        "shared_name" : "Ppfibp1 (-) mmu-miR-133b-3p",
        "name" : "Ppfibp1 (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 322,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "321",
        "source" : "139",
        "target" : "87",
        "shared_name" : "Srpr (-) mmu-miR-499-5p",
        "name" : "Srpr (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 321,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "320",
        "source" : "138",
        "target" : "77",
        "shared_name" : "Myo5a (-) mmu-miR-20a-5p",
        "name" : "Myo5a (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 320,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "319",
        "source" : "138",
        "target" : "110",
        "shared_name" : "Myo5a (-) mmu-miR-154-5p",
        "name" : "Myo5a (-) mmu-miR-154-5p",
        "interaction" : "-",
        "SUID" : 319,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "318",
        "source" : "138",
        "target" : "63",
        "shared_name" : "Myo5a (-) mmu-miR-22-3p",
        "name" : "Myo5a (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 318,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "317",
        "source" : "138",
        "target" : "87",
        "shared_name" : "Myo5a (-) mmu-miR-499-5p",
        "name" : "Myo5a (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 317,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "316",
        "source" : "138",
        "target" : "103",
        "shared_name" : "Myo5a (-) mmu-miR-1a-3p",
        "name" : "Myo5a (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 316,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "315",
        "source" : "137",
        "target" : "116",
        "shared_name" : "Ptbp3 (-) mmu-miR-133b-3p",
        "name" : "Ptbp3 (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 315,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "314",
        "source" : "137",
        "target" : "87",
        "shared_name" : "Ptbp3 (-) mmu-miR-499-5p",
        "name" : "Ptbp3 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 314,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "313",
        "source" : "137",
        "target" : "103",
        "shared_name" : "Ptbp3 (-) mmu-miR-1a-3p",
        "name" : "Ptbp3 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 313,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "312",
        "source" : "136",
        "target" : "116",
        "shared_name" : "Myo9b (-) mmu-miR-133b-3p",
        "name" : "Myo9b (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 312,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "311",
        "source" : "136",
        "target" : "103",
        "shared_name" : "Myo9b (-) mmu-miR-1a-3p",
        "name" : "Myo9b (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 311,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "310",
        "source" : "136",
        "target" : "77",
        "shared_name" : "Myo9b (-) mmu-miR-20a-5p",
        "name" : "Myo9b (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 310,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "309",
        "source" : "135",
        "target" : "63",
        "shared_name" : "Lpin2 (-) mmu-miR-22-3p",
        "name" : "Lpin2 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 309,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "308",
        "source" : "134",
        "target" : "103",
        "shared_name" : "B3galnt2 (-) mmu-miR-1a-3p",
        "name" : "B3galnt2 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 308,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "307",
        "source" : "133",
        "target" : "103",
        "shared_name" : "Slc25a12 (-) mmu-miR-1a-3p",
        "name" : "Slc25a12 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 307,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "306",
        "source" : "132",
        "target" : "63",
        "shared_name" : "Vamp1 (-) mmu-miR-22-3p",
        "name" : "Vamp1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 306,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "305",
        "source" : "132",
        "target" : "103",
        "shared_name" : "Vamp1 (-) mmu-miR-1a-3p",
        "name" : "Vamp1 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 305,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "304",
        "source" : "131",
        "target" : "77",
        "shared_name" : "Golm1 (-) mmu-miR-20a-5p",
        "name" : "Golm1 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 304,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "303",
        "source" : "130",
        "target" : "116",
        "shared_name" : "Zfp697 (-) mmu-miR-133b-3p",
        "name" : "Zfp697 (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 303,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "302",
        "source" : "129",
        "target" : "103",
        "shared_name" : "Tns3 (-) mmu-miR-1a-3p",
        "name" : "Tns3 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 302,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "301",
        "source" : "128",
        "target" : "63",
        "shared_name" : "Smad3 (-) mmu-miR-22-3p",
        "name" : "Smad3 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 301,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "300",
        "source" : "127",
        "target" : "116",
        "shared_name" : "Ahnak (-) mmu-miR-133b-3p",
        "name" : "Ahnak (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 300,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "299",
        "source" : "127",
        "target" : "77",
        "shared_name" : "Ahnak (-) mmu-miR-20a-5p",
        "name" : "Ahnak (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 299,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "298",
        "source" : "126",
        "target" : "116",
        "shared_name" : "Got1 (-) mmu-miR-133b-3p",
        "name" : "Got1 (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 298,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "297",
        "source" : "125",
        "target" : "63",
        "shared_name" : "Erbb3 (-) mmu-miR-22-3p",
        "name" : "Erbb3 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 297,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "296",
        "source" : "124",
        "target" : "103",
        "shared_name" : "Eid1 (-) mmu-miR-1a-3p",
        "name" : "Eid1 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 296,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "295",
        "source" : "123",
        "target" : "116",
        "shared_name" : "Tnc (-) mmu-miR-133b-3p",
        "name" : "Tnc (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 295,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "294",
        "source" : "123",
        "target" : "77",
        "shared_name" : "Tnc (-) mmu-miR-20a-5p",
        "name" : "Tnc (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 294,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "293",
        "source" : "122",
        "target" : "77",
        "shared_name" : "Dnaja2 (-) mmu-miR-20a-5p",
        "name" : "Dnaja2 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 293,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "292",
        "source" : "121",
        "target" : "87",
        "shared_name" : "Trp53bp1 (-) mmu-miR-499-5p",
        "name" : "Trp53bp1 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 292,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "291",
        "source" : "120",
        "target" : "87",
        "shared_name" : "Ric8 (-) mmu-miR-499-5p",
        "name" : "Ric8 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 291,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "290",
        "source" : "119",
        "target" : "103",
        "shared_name" : "Matr3 (-) mmu-miR-1a-3p",
        "name" : "Matr3 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 290,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "289",
        "source" : "118",
        "target" : "116",
        "shared_name" : "Tmem86a (-) mmu-miR-133b-3p",
        "name" : "Tmem86a (-) mmu-miR-133b-3p",
        "interaction" : "-",
        "SUID" : 289,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "288",
        "source" : "118",
        "target" : "63",
        "shared_name" : "Tmem86a (-) mmu-miR-22-3p",
        "name" : "Tmem86a (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 288,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "287",
        "source" : "118",
        "target" : "87",
        "shared_name" : "Tmem86a (-) mmu-miR-499-5p",
        "name" : "Tmem86a (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 287,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "286",
        "source" : "117",
        "target" : "63",
        "shared_name" : "Vim (-) mmu-miR-22-3p",
        "name" : "Vim (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 286,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "285",
        "source" : "116",
        "target" : "86",
        "shared_name" : "mmu-miR-133b-3p (-) Fam46a",
        "name" : "mmu-miR-133b-3p (-) Fam46a",
        "interaction" : "-",
        "SUID" : 285,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "284",
        "source" : "116",
        "target" : "68",
        "shared_name" : "mmu-miR-133b-3p (-) Csnk1g3",
        "name" : "mmu-miR-133b-3p (-) Csnk1g3",
        "interaction" : "-",
        "SUID" : 284,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "283",
        "source" : "116",
        "target" : "83",
        "shared_name" : "mmu-miR-133b-3p (-) Clcn3",
        "name" : "mmu-miR-133b-3p (-) Clcn3",
        "interaction" : "-",
        "SUID" : 283,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "282",
        "source" : "116",
        "target" : "79",
        "shared_name" : "mmu-miR-133b-3p (-) Foxp1",
        "name" : "mmu-miR-133b-3p (-) Foxp1",
        "interaction" : "-",
        "SUID" : 282,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "281",
        "source" : "116",
        "target" : "115",
        "shared_name" : "mmu-miR-133b-3p (-) Tmem127",
        "name" : "mmu-miR-133b-3p (-) Tmem127",
        "interaction" : "-",
        "SUID" : 281,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "280",
        "source" : "116",
        "target" : "69",
        "shared_name" : "mmu-miR-133b-3p (-) Cacnb1",
        "name" : "mmu-miR-133b-3p (-) Cacnb1",
        "interaction" : "-",
        "SUID" : 280,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "279",
        "source" : "116",
        "target" : "111",
        "shared_name" : "mmu-miR-133b-3p (-) Mtus1",
        "name" : "mmu-miR-133b-3p (-) Mtus1",
        "interaction" : "-",
        "SUID" : 279,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "278",
        "source" : "116",
        "target" : "72",
        "shared_name" : "mmu-miR-133b-3p (-) Fnip2",
        "name" : "mmu-miR-133b-3p (-) Fnip2",
        "interaction" : "-",
        "SUID" : 278,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "277",
        "source" : "116",
        "target" : "70",
        "shared_name" : "mmu-miR-133b-3p (-) Tcirg1",
        "name" : "mmu-miR-133b-3p (-) Tcirg1",
        "interaction" : "-",
        "SUID" : 277,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "276",
        "source" : "116",
        "target" : "67",
        "shared_name" : "mmu-miR-133b-3p (-) Cdca4",
        "name" : "mmu-miR-133b-3p (-) Cdca4",
        "interaction" : "-",
        "SUID" : 276,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "275",
        "source" : "116",
        "target" : "64",
        "shared_name" : "mmu-miR-133b-3p (-) Dlat",
        "name" : "mmu-miR-133b-3p (-) Dlat",
        "interaction" : "-",
        "SUID" : 275,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "274",
        "source" : "116",
        "target" : "100",
        "shared_name" : "mmu-miR-133b-3p (-) Arid5a",
        "name" : "mmu-miR-133b-3p (-) Arid5a",
        "interaction" : "-",
        "SUID" : 274,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "273",
        "source" : "116",
        "target" : "99",
        "shared_name" : "mmu-miR-133b-3p (-) Fat1",
        "name" : "mmu-miR-133b-3p (-) Fat1",
        "interaction" : "-",
        "SUID" : 273,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "272",
        "source" : "115",
        "target" : "63",
        "shared_name" : "Tmem127 (-) mmu-miR-22-3p",
        "name" : "Tmem127 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 272,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "271",
        "source" : "115",
        "target" : "103",
        "shared_name" : "Tmem127 (-) mmu-miR-1a-3p",
        "name" : "Tmem127 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 271,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "270",
        "source" : "115",
        "target" : "77",
        "shared_name" : "Tmem127 (-) mmu-miR-20a-5p",
        "name" : "Tmem127 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 270,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "269",
        "source" : "114",
        "target" : "77",
        "shared_name" : "Scrn3 (-) mmu-miR-20a-5p",
        "name" : "Scrn3 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 269,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "268",
        "source" : "113",
        "target" : "103",
        "shared_name" : "Gpc6 (-) mmu-miR-1a-3p",
        "name" : "Gpc6 (-) mmu-miR-1a-3p",
        "interaction" : "-",
        "SUID" : 268,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "267",
        "source" : "112",
        "target" : "63",
        "shared_name" : "Cdc37l1 (-) mmu-miR-22-3p",
        "name" : "Cdc37l1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 267,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "266",
        "source" : "112",
        "target" : "77",
        "shared_name" : "Cdc37l1 (-) mmu-miR-20a-5p",
        "name" : "Cdc37l1 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 266,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "265",
        "source" : "111",
        "target" : "87",
        "shared_name" : "Mtus1 (-) mmu-miR-499-5p",
        "name" : "Mtus1 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 265,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "264",
        "source" : "110",
        "target" : "84",
        "shared_name" : "mmu-miR-154-5p (-) Nono",
        "name" : "mmu-miR-154-5p (-) Nono",
        "interaction" : "-",
        "SUID" : 264,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "263",
        "source" : "110",
        "target" : "102",
        "shared_name" : "mmu-miR-154-5p (-) Homer1",
        "name" : "mmu-miR-154-5p (-) Homer1",
        "interaction" : "-",
        "SUID" : 263,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "262",
        "source" : "110",
        "target" : "73",
        "shared_name" : "mmu-miR-154-5p (-) Ar",
        "name" : "mmu-miR-154-5p (-) Ar",
        "interaction" : "-",
        "SUID" : 262,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "261",
        "source" : "110",
        "target" : "93",
        "shared_name" : "mmu-miR-154-5p (-) Hnrnpu",
        "name" : "mmu-miR-154-5p (-) Hnrnpu",
        "interaction" : "-",
        "SUID" : 261,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "260",
        "source" : "110",
        "target" : "76",
        "shared_name" : "mmu-miR-154-5p (-) Apbb2",
        "name" : "mmu-miR-154-5p (-) Apbb2",
        "interaction" : "-",
        "SUID" : 260,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "259",
        "source" : "110",
        "target" : "67",
        "shared_name" : "mmu-miR-154-5p (-) Cdca4",
        "name" : "mmu-miR-154-5p (-) Cdca4",
        "interaction" : "-",
        "SUID" : 259,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "258",
        "source" : "110",
        "target" : "101",
        "shared_name" : "mmu-miR-154-5p (-) Pkd1",
        "name" : "mmu-miR-154-5p (-) Pkd1",
        "interaction" : "-",
        "SUID" : 258,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "257",
        "source" : "110",
        "target" : "81",
        "shared_name" : "mmu-miR-154-5p (-) Maf",
        "name" : "mmu-miR-154-5p (-) Maf",
        "interaction" : "-",
        "SUID" : 257,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "256",
        "source" : "109",
        "target" : "63",
        "shared_name" : "Myo10 (-) mmu-miR-22-3p",
        "name" : "Myo10 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 256,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "255",
        "source" : "108",
        "target" : "63",
        "shared_name" : "Spop (-) mmu-miR-22-3p",
        "name" : "Spop (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 255,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "254",
        "source" : "108",
        "target" : "77",
        "shared_name" : "Spop (-) mmu-miR-20a-5p",
        "name" : "Spop (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 254,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "253",
        "source" : "107",
        "target" : "87",
        "shared_name" : "Atp2b1 (-) mmu-miR-499-5p",
        "name" : "Atp2b1 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 253,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "252",
        "source" : "106",
        "target" : "77",
        "shared_name" : "Cd99l2 (-) mmu-miR-20a-5p",
        "name" : "Cd99l2 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 252,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "251",
        "source" : "105",
        "target" : "63",
        "shared_name" : "Fam13b (-) mmu-miR-22-3p",
        "name" : "Fam13b (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 251,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "250",
        "source" : "104",
        "target" : "63",
        "shared_name" : "Arfip2 (-) mmu-miR-22-3p",
        "name" : "Arfip2 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 250,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "249",
        "source" : "103",
        "target" : "62",
        "shared_name" : "mmu-miR-1a-3p (-) Fam20a",
        "name" : "mmu-miR-1a-3p (-) Fam20a",
        "interaction" : "-",
        "SUID" : 249,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "248",
        "source" : "103",
        "target" : "102",
        "shared_name" : "mmu-miR-1a-3p (-) Homer1",
        "name" : "mmu-miR-1a-3p (-) Homer1",
        "interaction" : "-",
        "SUID" : 248,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "247",
        "source" : "103",
        "target" : "101",
        "shared_name" : "mmu-miR-1a-3p (-) Pkd1",
        "name" : "mmu-miR-1a-3p (-) Pkd1",
        "interaction" : "-",
        "SUID" : 247,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "246",
        "source" : "103",
        "target" : "100",
        "shared_name" : "mmu-miR-1a-3p (-) Arid5a",
        "name" : "mmu-miR-1a-3p (-) Arid5a",
        "interaction" : "-",
        "SUID" : 246,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "245",
        "source" : "103",
        "target" : "99",
        "shared_name" : "mmu-miR-1a-3p (-) Fat1",
        "name" : "mmu-miR-1a-3p (-) Fat1",
        "interaction" : "-",
        "SUID" : 245,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "244",
        "source" : "103",
        "target" : "97",
        "shared_name" : "mmu-miR-1a-3p (-) Rtn4",
        "name" : "mmu-miR-1a-3p (-) Rtn4",
        "interaction" : "-",
        "SUID" : 244,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "243",
        "source" : "103",
        "target" : "96",
        "shared_name" : "mmu-miR-1a-3p (-) Atl2",
        "name" : "mmu-miR-1a-3p (-) Atl2",
        "interaction" : "-",
        "SUID" : 243,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "242",
        "source" : "103",
        "target" : "93",
        "shared_name" : "mmu-miR-1a-3p (-) Hnrnpu",
        "name" : "mmu-miR-1a-3p (-) Hnrnpu",
        "interaction" : "-",
        "SUID" : 242,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "241",
        "source" : "103",
        "target" : "91",
        "shared_name" : "mmu-miR-1a-3p (-) Ndufa8",
        "name" : "mmu-miR-1a-3p (-) Ndufa8",
        "interaction" : "-",
        "SUID" : 241,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "240",
        "source" : "103",
        "target" : "89",
        "shared_name" : "mmu-miR-1a-3p (-) Srr",
        "name" : "mmu-miR-1a-3p (-) Srr",
        "interaction" : "-",
        "SUID" : 240,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "239",
        "source" : "103",
        "target" : "88",
        "shared_name" : "mmu-miR-1a-3p (-) Aldh5a1",
        "name" : "mmu-miR-1a-3p (-) Aldh5a1",
        "interaction" : "-",
        "SUID" : 239,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "238",
        "source" : "103",
        "target" : "83",
        "shared_name" : "mmu-miR-1a-3p (-) Clcn3",
        "name" : "mmu-miR-1a-3p (-) Clcn3",
        "interaction" : "-",
        "SUID" : 238,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "237",
        "source" : "103",
        "target" : "80",
        "shared_name" : "mmu-miR-1a-3p (-) 2610008E11Rik",
        "name" : "mmu-miR-1a-3p (-) 2610008E11Rik",
        "interaction" : "-",
        "SUID" : 237,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "236",
        "source" : "103",
        "target" : "79",
        "shared_name" : "mmu-miR-1a-3p (-) Foxp1",
        "name" : "mmu-miR-1a-3p (-) Foxp1",
        "interaction" : "-",
        "SUID" : 236,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "235",
        "source" : "103",
        "target" : "74",
        "shared_name" : "mmu-miR-1a-3p (-) Zdhhc18",
        "name" : "mmu-miR-1a-3p (-) Zdhhc18",
        "interaction" : "-",
        "SUID" : 235,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "234",
        "source" : "103",
        "target" : "98",
        "shared_name" : "mmu-miR-1a-3p (-) Htt",
        "name" : "mmu-miR-1a-3p (-) Htt",
        "interaction" : "-",
        "SUID" : 234,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "233",
        "source" : "103",
        "target" : "66",
        "shared_name" : "mmu-miR-1a-3p (-) Chst11",
        "name" : "mmu-miR-1a-3p (-) Chst11",
        "interaction" : "-",
        "SUID" : 233,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "232",
        "source" : "102",
        "target" : "63",
        "shared_name" : "Homer1 (-) mmu-miR-22-3p",
        "name" : "Homer1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 232,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "231",
        "source" : "101",
        "target" : "63",
        "shared_name" : "Pkd1 (-) mmu-miR-22-3p",
        "name" : "Pkd1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 231,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "230",
        "source" : "101",
        "target" : "77",
        "shared_name" : "Pkd1 (-) mmu-miR-20a-5p",
        "name" : "Pkd1 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 230,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "229",
        "source" : "99",
        "target" : "77",
        "shared_name" : "Fat1 (-) mmu-miR-20a-5p",
        "name" : "Fat1 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 229,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "228",
        "source" : "99",
        "target" : "87",
        "shared_name" : "Fat1 (-) mmu-miR-499-5p",
        "name" : "Fat1 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 228,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "227",
        "source" : "98",
        "target" : "63",
        "shared_name" : "Htt (-) mmu-miR-22-3p",
        "name" : "Htt (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 227,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "226",
        "source" : "96",
        "target" : "77",
        "shared_name" : "Atl2 (-) mmu-miR-20a-5p",
        "name" : "Atl2 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 226,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "225",
        "source" : "95",
        "target" : "77",
        "shared_name" : "C2cd2 (-) mmu-miR-20a-5p",
        "name" : "C2cd2 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 225,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "224",
        "source" : "94",
        "target" : "63",
        "shared_name" : "Tgfbr1 (-) mmu-miR-22-3p",
        "name" : "Tgfbr1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 224,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "223",
        "source" : "94",
        "target" : "87",
        "shared_name" : "Tgfbr1 (-) mmu-miR-499-5p",
        "name" : "Tgfbr1 (-) mmu-miR-499-5p",
        "interaction" : "-",
        "SUID" : 223,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "222",
        "source" : "93",
        "target" : "77",
        "shared_name" : "Hnrnpu (-) mmu-miR-20a-5p",
        "name" : "Hnrnpu (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 222,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "221",
        "source" : "92",
        "target" : "77",
        "shared_name" : "Ggnbp1 (-) mmu-miR-20a-5p",
        "name" : "Ggnbp1 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 221,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "220",
        "source" : "90",
        "target" : "63",
        "shared_name" : "Cyfip1 (-) mmu-miR-22-3p",
        "name" : "Cyfip1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 220,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "219",
        "source" : "87",
        "target" : "85",
        "shared_name" : "mmu-miR-499-5p (-) Reep1",
        "name" : "mmu-miR-499-5p (-) Reep1",
        "interaction" : "-",
        "SUID" : 219,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "218",
        "source" : "87",
        "target" : "81",
        "shared_name" : "mmu-miR-499-5p (-) Maf",
        "name" : "mmu-miR-499-5p (-) Maf",
        "interaction" : "-",
        "SUID" : 218,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "217",
        "source" : "87",
        "target" : "82",
        "shared_name" : "mmu-miR-499-5p (-) Arpp21",
        "name" : "mmu-miR-499-5p (-) Arpp21",
        "interaction" : "-",
        "SUID" : 217,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "216",
        "source" : "87",
        "target" : "76",
        "shared_name" : "mmu-miR-499-5p (-) Apbb2",
        "name" : "mmu-miR-499-5p (-) Apbb2",
        "interaction" : "-",
        "SUID" : 216,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "215",
        "source" : "87",
        "target" : "72",
        "shared_name" : "mmu-miR-499-5p (-) Fnip2",
        "name" : "mmu-miR-499-5p (-) Fnip2",
        "interaction" : "-",
        "SUID" : 215,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "214",
        "source" : "83",
        "target" : "63",
        "shared_name" : "Clcn3 (-) mmu-miR-22-3p",
        "name" : "Clcn3 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 214,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "213",
        "source" : "81",
        "target" : "77",
        "shared_name" : "Maf (-) mmu-miR-20a-5p",
        "name" : "Maf (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 213,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "212",
        "source" : "80",
        "target" : "63",
        "shared_name" : "2610008E11Rik (-) mmu-miR-22-3p",
        "name" : "2610008E11Rik (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 212,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "211",
        "source" : "80",
        "target" : "77",
        "shared_name" : "2610008E11Rik (-) mmu-miR-20a-5p",
        "name" : "2610008E11Rik (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 211,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "210",
        "source" : "79",
        "target" : "63",
        "shared_name" : "Foxp1 (-) mmu-miR-22-3p",
        "name" : "Foxp1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 210,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "209",
        "source" : "78",
        "target" : "77",
        "shared_name" : "Prkab2 (-) mmu-miR-20a-5p",
        "name" : "Prkab2 (-) mmu-miR-20a-5p",
        "interaction" : "-",
        "SUID" : 209,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "208",
        "source" : "77",
        "target" : "76",
        "shared_name" : "mmu-miR-20a-5p (-) Apbb2",
        "name" : "mmu-miR-20a-5p (-) Apbb2",
        "interaction" : "-",
        "SUID" : 208,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "207",
        "source" : "77",
        "target" : "75",
        "shared_name" : "mmu-miR-20a-5p (-) Ccng1",
        "name" : "mmu-miR-20a-5p (-) Ccng1",
        "interaction" : "-",
        "SUID" : 207,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "206",
        "source" : "77",
        "target" : "71",
        "shared_name" : "mmu-miR-20a-5p (-) Sar1b",
        "name" : "mmu-miR-20a-5p (-) Sar1b",
        "interaction" : "-",
        "SUID" : 206,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "205",
        "source" : "77",
        "target" : "67",
        "shared_name" : "mmu-miR-20a-5p (-) Cdca4",
        "name" : "mmu-miR-20a-5p (-) Cdca4",
        "interaction" : "-",
        "SUID" : 205,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "204",
        "source" : "70",
        "target" : "63",
        "shared_name" : "Tcirg1 (-) mmu-miR-22-3p",
        "name" : "Tcirg1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 204,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "203",
        "source" : "69",
        "target" : "63",
        "shared_name" : "Cacnb1 (-) mmu-miR-22-3p",
        "name" : "Cacnb1 (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 203,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "202",
        "source" : "65",
        "target" : "63",
        "shared_name" : "Adsl (-) mmu-miR-22-3p",
        "name" : "Adsl (-) mmu-miR-22-3p",
        "interaction" : "-",
        "SUID" : 202,
        "shared_interaction" : "-",
        "selected" : false
      },
      "selected" : false
    } ]
  }
}}