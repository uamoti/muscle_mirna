#!/bin/bash
# SLURM parameters
#SBATCH -t 00:30:00
#SBATCH -N 2
#SBATCH --tasks-per-node=20
#SBATCH -J bt_align
#SBATCH -o bt_align_%j.out
#SBATCH -e bt_align_%j.err

# Declare the directories needed
SAM_PATH=/lunarc/nobackup/users/bernardo/up_121/sam/
SORT_PATH=/lunarc/nobackup/users/bernardo/up_121/sam/sorted/
cd $SAM_PATH

for sam in $(ls); do
    SAM_NAME=$(basename $sam .sam)
    samtools sort -n $sam -o $SORT_PATH$SAM_NAME.sort.sam
done
