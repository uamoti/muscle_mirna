#!/bin/bash
# SLURM parameters
#SBATCH -t 00:30:00
#SBATCH -N 1
#SBATCH -J bam2fq_072
#SBATCH -o log/bam2fq_072_%j.out
#SBATCH -e log/bam2fq_072_%j.err

# Load modules
module load GCC/5.4.0-2.26
module load OpenMPI/1.10.3
module load BEDTools/2.26.0

# Declare the directories needed
BAM_PATH=/lunarc/nobackup/users/bernardo/up_072/bams/
FASTQ_PATH=/lunarc/nobackup/users/bernardo/up_072/fastq/

if [ ! -d $FASTQ_PATH ]; then
    mkdir $FASTQ_PATH
fi

cd $BAM_PATH
BAM_LIST=$(ls | grep -e 'bam$')

# Convert BAM to FASTQ
for bam in $BAM_LIST; do
    FQ_NAME=$(basename $bam .bam | sed 's/_mapped_to_mm10//')
    bedtools bamtofastq -i $bam -fq $FASTQ_PATH/$FQ_NAME.fastq
done

