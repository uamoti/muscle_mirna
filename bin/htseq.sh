#!/bin/bash
# SLURM parameters
#SBATCH -t 00:45:00
#SBATCH -N 1
#SBATCH -J htseq_072
#SBATCH -o log/htseq_072_%j.out
#SBATCH -e log/htseq_072_%j.err

# Load modules
module load GCC/5.4.0-2.26
module load OpenMPI/1.10.3
module load HTSeq/0.9.1-Python-2.7.12

# Declare the directories needed
SAM_PATH=/lunarc/nobackup/users/bernardo/up_072/sams/mature/
GTF_PATH=/lunarc/nobackup/users/bernardo/gtf/mmu_miRNA_newcoord_nodoublon.gff3
HTS_OUT=/lunarc/nobackup/users/bernardo/up_072/hts_count/mature

cd $SAM_PATH

if [ ! -d $HTS_OUT ]; then
    mkdir $HTS_OUT
fi

for sam in $(ls *.sam); do
    NAME=$(basename $sam .sam)
    python -m HTSeq.scripts.count -t miRNA -i Name -a 0 -s yes $sam $GTF_PATH \
        > $HTS_OUT$NAME.tsv
done
