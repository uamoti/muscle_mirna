#!/bin/bash
# SLURM parameters
#SBATCH -t 00:30:00
#SBATCH -N 1
#SBATCH -J cutadapt_072
#SBATCH -o cutadapt_072_%j.out
#SBATCH -e cutadapt_072_%j.err

# Load modules
module load GCC/4.9.3-2.25
module laod OpenMPI/1.10.2
module load cutadapt/1.9.1-Python-2.7.11

# Declare the directories needed
FASTQ_PATH=/lunarc/nobackup/users/bernardo/up_072/fastq/
FILT_PATH=/lunarc/nobackup/users/bernardo/up_072/fastq/mir_length/

cd $FASTQ_PATH

if [ ! -d $FILT_PATH ]; then
    mkdir $FILT_PATH
fi

for fq in $(ls *.fastq); do
    NAME=$(basename $fq .fastq)
    cutadapt -m 18 -M 25 -o $FILT_PATH$NAME.fastq $fq
done
