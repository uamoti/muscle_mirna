#!/bin/bash
# SLURM parameters
#SBATCH -t 00:10:00
#SBATCH -N 1
#SBATCH -J sam2bam_072
#SBATCH -o log/sam2bam_072_%j.out
#SBATCH -e log/sam2bam_072_%j.err

# load modules
module load icc/2017.1.132-GCC-6.3.0-2.27
module load impi/2017.1.132
module load SAMtools/1.4.1

# file paths
SAM_PATH=/lunarc/nobackup/users/bernardo/up_072/sams/
BAM_PATH=/lunarc/nobackup/users/bernardo/up_072/bams/mature/

cd $SAM_PATH

# convert
for sam in $(ls); do
    BAM_NAME=$(basename $sam .sam)
    samtools view -b -h -o $BAM_PATH$BAM_NAME.bam $sam
done
