#!/bin/bash
# SLURM parameters
#SBATCH -t 01:00:00
#SBATCH -N 1
#SBATCH --tasks-per-node 20
#SBATCH -J bt_align_072
#SBATCH -o bt_align_%j.out
#SBATCH -e bt_align_%j.err

# Load modules
module load icc/2015.3.187-GNU-4.9.3-2.25
module load impi/5.0.3.048
module load Bowtie/1.1.2

# Declare the file paths
FASTQ_PATH=/lunarc/nobackup/users/bernardo/up_072/fastq/mir_length/
SAM_PATH=/lunarc/nobackup/users/bernardo/up_072/sams/mature/

# Tell bowtie where to find its indexes
EBWT=mmu_mir_bt
export BOWTIE_INDEXES=/lunarc/nobackup/users/bernardo/ebwt/

cd $FASTQ_PATH

if [ ! -d $SAM_PATH ]; then
    mkdir $SAM_PATH
fi

# Align
for fq in $(ls *.fastq); do
    SAM_NAME=$(basename $fq .fastq)
    bowtie --verbose -p 20 -S -l 8 -n 0 -a --best --strata $EBWT $fq \
    > $SAM_PATH$SAM_NAME.sam
done
