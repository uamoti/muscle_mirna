# import packages/modules
import gseapy as gp
import goatools as go
import goatools.associations as goa
import pandas as pd
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
#import networkx as nx

# versions
print('pandas', pd.__version__, '\n',
      'gseapy', gp.__version__, '\n',
      'goatools', go.__version__, '\n',
      'numpy', np.__version__, '\n',
      'scipy', sp.__version__, '\n',
      'networkx', nx.__version__)
      
# read DESeq2 results
de = pd.read_table('Dados/deseq_results.tsv', header=0, sep='\s+')
# subset df for significant miRNAs at the 5% level and absolute log2FC >= 1
# we'll also split it into up- and down-regulated miRNAs
up_mir_df = de[(de.padj <= 0.05) & (de.log2FoldChange >= 1)]
down_mir_df = de[(de.padj <= 0.05) & (de.log2FoldChange <= -1)]
# DE miR names
sig_mir = up_mir_df.index.union(down_mir_df.index)
# number of DE miRNAs
print('Number of differentially expressed miRNA:', len(sig_mir), '\n')
# top-20 up- and down-regulated miRNAs sorted by log2FoldChange, with a minimum
# average expression across samples of 10
print('Top-20 up-regulated miRNAs')
print(up_mir_df[up_mir_df.baseMean > 10].sort_values(by='log2FoldChange', ascending=False).head(20), '\n')
print('Top-20 down-regulated miRNAs')
print(down_mir_df[down_mir_df.baseMean > 10].sort_values(by='log2FoldChange').head(20))

'''Read database of mouse miRNA targets. Currently using the skeletal muscle
subset of Tarbase in this example; doing so gives us 12 unique miRNAs from the
DE ones, if we use the whole database we get 62 unique miRNAs; will probably use
the whole database later'''

tar = pd.read_table('Dados/mouse_muscle_tar.tsv', header=0)
# subset Tarbase to only DE miRNAs
mask = [m in sig_mir for m in tar.mirna]
sig_tar = tar.loc[mask]

# we'll generate a pandas Series with DE miRNAs as labels and sets of their
# target genes as values
targets = sig_tar.groupby('mirna').geneName.apply(set)
# check the number of targets per miRNA
print('DE miRNAs target numbers\n', targets.apply(len))
# remember these are all the possible targets
# get a flat list of genes for later use with Enrich
targ_arr = np.concatenate([list(x) for x in targets])
# remove duplicates by turning into a set
targ_arr = set(targ_arr)

## ENRICHR SIMULATION WITH POTENTIAL MIRNA TARGETS
# list of possible sets to query
gp.get_library_name()
# query the Enrichr API for KEGG_2016 gene sets
# gseapy won't accept a numpy array as gene list so be sure to convert it to
# a list or pandas Series/DataFrame
enr_sim = gp.enrichr(gene_list=list(targets), description='mir_targets_muscle_tar',
                     gene_sets='KEGG_2016', outdir='gseapy_kegg', cutoff=0.1,
                     no_plot=True)

# get the results as a data frame
enr_sim_df = enr_sim.res2d
# number of significant terms at the 10% level
len(enr_sim_df[enr_sim_df['Adjusted P-value'] < 0.1])
# view some significant terms
enr_sim_df[enr_sim_df['Adjusted P-value'] < 0.1]['Term'][:20]
# to view by increasing p-value
enr_sim_df.sort_values('Adjusted P-value')['Term'][:20]
# all significant terms
print(enr_sim_df[enr_sim_df['Adjusted P-value'] < 0.1]['Term'])

## ENRICHR WITH MICROARRAY DATA
# repeat the enrichment analysis with real mRNA data from previous micro-array study
# remove the usecols parameter if you want the whole table
gene_table = pd.read_table('Dados/microarray.csv', usecols=['p.value.adj', 
                           'Genes.Symbol', 'Genes.Name', 'Genes.Ensembl'],
                           header=0)
# significant genes at the 10% level
sig_genes = gene_table[gene_table['p.value.adj'] < 0.1]['Genes.Symbol'].dropna().unique()
# run enrichment
enr = gp.enrichr(gene_list=list(sig_genes), description='dy3K_quad_genes',
                 gene_sets='KEGG_2016', outdir='gseapy_kegg', cutoff=0.1,
                 no_plot=True)

# get the results as a data frame
enr_df = enr.res2d
# number of significant terms at the 10% level
len(enr_df[enr_df['Adjusted P-value'] < 0.1])

# run the traditional GSEA workflow
# specify the gene set we'll probe
gmt = 'Dados/c2.all.v6.0.symbols.gmt'
# phenotype file
pheno = 'Dados/gsea_pheno.cls'
gsea_table = pd.read_table('Dados/gseapy_expression_table.tsv', header=0)
# run set analysis; this is a more intense computation so I ran it separately.
# I'll read the results into a data frame; uncomment the next line to run GSEA
#gse_res = gp.gsea(data=gsea_table, gene_sets=gmt, cls=pheno, permutation_type=
#              'phenotype', min_size=10, outdir='gsea_c2')
#gp_df = gse_res.res2d
gp_df = pd.read_table('Dados/gseapy_c2.tsv', header=0, decimal=',')

# number of significant terms at the 10% level
len(gp_df[gp_df['fdr'] < 0.1])
# view some significant terms
gp_df[gp_df['fdr'] < 0.1]['Term'][:20]
# to view by increasing p-value
gp_df.sort_values('fdr')['Term'][:20]
# all significant terms at 10% level
print(gp_df[gp_df['fdr'] < 0.1]['Term'])

## GO ENRICHMENT OF MICROARRAY DATA

mapping = 'Dados/mmu_go_associations.txt'
obo_file = 'Dados/go-basic.obo'
# load ontologies
obo = go.obo_parser.GODag(obo_file)
# load associations
go2gene = goa.read_associations(mapping)
# all study genes as background set
bkg_genes = gene_table['Genes.Symbol']
# initialise GO Enrichment Analysis
goea = go.go_enrichment.GOEnrichmentStudy(bkg_genes, go2gene, obo)
# run the analysis
go_res = goea.run_study(sig_genes)
# significant results
go_res_sig = [r for r in go_res if r.p_bonferroni < 0.1]

''' The GO results in the above lists are tab-separated. Pandas' data frame
constructor won't be able to make a df out-of-the-box. We'll turn each GO record
into another list by splitting at the tab character. We'll limit to the
significant results'''

go_sig_str = [r.__str__().split('\t') for r in go_res_sig]
# get GO enrichment columns
go_cols = go_res[0].get_prtflds_default()
# create a table
go_sig_tab = pd.DataFrame(go_sig_str, columns=go_cols)
go_sig_tab.to_csv('go_microarray.tsv', sep='\t')
# some significant terms
go_sig_tab.head(10)

## MRNA-MIRNA INTERACTION
'''The differentially expressed genes overlap substantially with some gene sets
that make sense considering the tissue and disease. A couple of metabolism-related
terms top the list, which is in line with our previous proteomics study (de Oliveira et al. 2014, Mol Cell Proteomics).
We're now equiped with both a microRNA and an mRNA data set, so we have the chance
to probe the direct (and inverse) interaction between miRNA-mRNA, i.e. if a miRNA
is up-regulated then its targets should be down-regulated.'''

# the gene table was previously split into up- and down-regulated gene tables,
# we'll read these now and save into pandas Series for easier manipulation
up_genes = pd.read_table('Dados/genes_up.tsv', header=0,
                         usecols=['Genes_Symbol'],decimal=',')
up_genes = pd.Series(up_genes.Genes_Symbol)
up_genes.dropna(inplace=True)
# number of up-regulated genes
len(up_genes)
down_genes = pd.read_table('Dados/genes_down.tsv', header=0,
                           usecols=['Genes_Symbol'], decimal=',')
down_genes = pd.Series(down_genes.Genes_Symbol)
down_genes.dropna(inplace=True)
# number of down-regulated genes
len(down_genes)
# the same for miRNAs
up_mir = sig_mir_df[sig_mir_df.log2FoldChange >= 1].index
len(up_mir)
down_mir = sig_mir_df[sig_mir_df.log2FoldChange <= -1].index
len(down_mir)
up_tar = {} # dictionary of up-miR/down-mRNA
# up-regulated miRNAs and down-regulated genes
for mir in up_mir:
    tars = tar[tar.mirna == mir].geneName.dropna().unique()
    confirmed = set(tars).intersection(set(down_genes))
    if len(confirmed) > 0:
#        print('--> miR', mir, 'significant targets:', confirmed)
        up_tar[mir] = confirmed

# number of miRNAs with DE target genes
len(up_tar)
down_tar = {}
# down-regulated miRNAs and up-regulated genes
for mir in down_mir:
    tars = tar[tar.mirna == mir].geneName.dropna().unique()
    confirmed = set(tars).intersection(set(up_genes))
    if len(confirmed) > 0:
#        print('--> miR', mir, 'significant targets:', confirmed)
        down_tar[mir] = confirmed

len(down_tar)

# the dictionaries we've created allow us to do some calculations
# we can, for example, rank the miRNAs according to number of target genes
# up-regulated miRNAs and down-regulated mRNA
print('Rank of up-regulated miRNAs by number of target genes\n')
pd.Series(up_tar).apply(len).sort_values(ascending=False)
# now the inverse
print('Rank of down-regulated miRNAs by number of target genes\n')
pd.Series(down_tar).apply(len).sort_values(ascending=False)

# likewise, we can calculate in reverse, i.e. the number of miRNA that target a
# certain gene
up_mrna = {}
down_mrna = {}

for gene in down_genes:
    mirs = tar[tar.geneName == gene].mirna.dropna().unique()
    confirmed = set(mirs).intersection(set(up_mir))
    if len(confirmed) > 0:
#        print('--> gene', gene, 'significant miRs:', confirmed)
        down_mrna[gene] = confirmed

for gene in up_genes:
    mirs = tar[tar.geneName == gene].mirna.dropna().unique()
    confirmed = set(mirs).intersection(set(down_mir))
    if len(confirmed) > 0:
#        print('--> gene', gene, 'significant miRs:', confirmed)
        up_mrna[gene] = confirmed

# get information about the target genes from NCBI
# we need to provide NCBI with an e-mail address
Entrez.email = 'dok14bmo@student.lu.se'
# down-regulated targets
for t in down_targets:
    query = '{}[Gene Symbol] AND "Mus musculus"[Organism]'.format(t)
    handle = Entrez.esearch(db='gene', term=query, retmax=1)
    rec = Entrez.read(handle)
    uid = rec['IdList'][0]
    handle = Entrez.efetch(db='gene', id=uid, rettype='gb', retmode='text')
    print(handle.read())

# the same for up-regulated targets
for t in up_targets:
    query = '{}[Gene Symbol] AND "Mus musculus"[Organism]'.format(t)
    handle = Entrez.esearch(db='gene', term=query, retmax=1)
    rec = Entrez.read(handle)
    uid = rec['IdList'][0]
    handle = Entrez.efetch(db='gene', id=uid, rettype='gb', retmode='text')
    print(handle.read())

print('Rank of down-regulated mRNAs by number of miRNAs that target it\n')
print(pd.Series(down_mrna).apply(len).sort_values(ascending=False))
print('Rank of up-regulated mRNAs by number of miRNAs that target it\n')
print(pd.Series(up_mrna).apply(len).sort_values(ascending=False))

### NETWORK
#'''Despite having the summary statistics above, it is usually interesting, although
#not always informative, to visualise genes as a network. To do so we'll use the
#nextorkx package. This package is meant for network analysis and not so much
#drawing. Better looking images can be generated with bokeh or plotly.'''
## we'll start with the up-regulated miRNAs and their target genes
#G = nx.Graph(up_tar)
#nx.draw(G, with_labels=True)
#plt.show()
